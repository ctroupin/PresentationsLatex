\documentclass[final,svgnames]{beamer}
\usepackage{tcolorbox}
\usepackage[orientation=portrait,scale=.95]{beamerposter}
%\usepackage{blindtext}

\usepackage{array}
\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{R}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{P}{>{\raggedright\arraybackslash}X}

\newcommand{\yestab}{{\cellcolor{LightGreen} Yes}}

\mode<presentation>
{
  \usetheme{IMDISposter}
}

\hypersetup{bookmarksopen=true,
bookmarksnumbered=true,  
pdffitwindow=false, 
pdfstartview=FitH,
pdftoolbar=true,
pdfmenubar=true,
pdfwindowui=true,
pdfsubject={IMDIS2018},
pdfkeywords={Diva, SeaDataCloud, ODIP, Jupyter, Zenodo, GitHub},
bookmarksopenlevel=1,
colorlinks=true,urlcolor=bluegher,
citecolor=bluegher}

\graphicspath{
{../../../figures4presentations/logo/},
{./figures/}
}

\usetikzlibrary{arrows,shapes,backgrounds,spy,calc}
\tikzstyle{every picture}+=[remember picture]
\tikzstyle{na} = [baseline=-.5ex]

\tikzset{
    vertex/.style = {
        circle,
        fill = black,
        outer sep = 0pt,
        inner sep = 1pt,
    }
}

\title[Scientific results traceability]{Scientific results traceability: software citation\\ using GitHub and Zenodo}
\author{Charles Troupin, Cristian Mu\~{n}oz, Juan Gabriel Fern\'{a}ndez and Miquel \`{A}ngel R\'{u}jula}
\institute{GHER (ULiège) and SOCIB}
\date{November 5-7, 2018}


\begin{document}
\begin{frame}[fragile]{} 
\centering
%\vspace{-1cm}
{\tcbset
{colframe=bluegher,nobeforeafter,colupper=white,colback=bluegher,arc=1mm,boxsep=3pt,height=1.5cm,center title}
\raisebox{8pt}{{\LARGE \faTags}}~~\tcbox{Oceanography}~\tcbox{Data analysis}~\tcbox{Data citation}~\tcbox{Reproducibility}~\tcbox{SeaDataCloud}~\tcbox{ODIP}~\hspace{1cm}\raisebox{10pt}{{\LARGE \faWrench}}~~\tcbox{Python}~\tcbox{Jupyter}~\tcbox{GitHub}~\tcbox{Zenodo}~\tcbox{orcID}
}

\rule{\columnwidth}{.2cm}
  
\begin{columns}[T]
  
% 1st column %-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\begin{column}{.31\linewidth}

\begin{block}{What will we talk about~\faQuestionCircle}
\justifying
\begin{description}
\item[\aiDoi] Digital Object Idenfitier: unique alphanumeric string assigned to identify content and provide a persistent link to its location on the Internet.
\item[\faGithub] GitHub: web-based hosting service for version control using \texttt{git}\hfill \url{http://github.com/}
\item[\aiOrcid] ORCID: persistent digital identifier to distinguishes researchers\hfill \url{https://orcid.org/}
\item[Zenodo]: a repository to deposit scientific papers and/or research data\hfill \url{https://zenodo.org/}
\end{description}

\end{block}

\begin{block}{How can we foster reproducibility~\faQuestionCircle}
\justifying
By making $\left\{\begin{tabular}{l}
Data\\
Method\\
Results\\
\end{tabular}\right\}$ available and citable

\begin{figure}
\begin{tikzpicture}[scale=2]
\node[text=black] (Data)  at (1,3) {Data};
\node[text=black] (Results)  at (1,1) {Results};
\node[text=black] (Methods)  at (1,2) {Methods};
\node[text=black] (Authors)  at (1,0) {Authors};
\node[text=black,right] (Repositories)  at (3,4) {Repositories};
\node[text=black,right] (Catalog)  at (3,1) {Catalogs};
\node[text=black,right] (Journal)  at (3,3) {Journals};
\node[text=black,right] (Orcid)  at (3,0) {Unique IDs};

\node[text=black,right] (J1)  at (7,3) {Earth System Science Data};
\node[text=black,right] (J2)  at (7,3.5) {Scientific Data};
\node[text=black,right] (J3)  at (7,2.5) {\ldots};

\node[text=DarkGray] (R1)  at (1,5.5) {What?};
\node[text=DarkGray,right] (R1)  at (3,5.5) {Where};
\node[text=DarkGray,right] (R1)  at (7,5.5) {Examples};

\node[text=black,right] (R1)  at (7,5.) {\includegraphics[height=.8cm]{logo_seadatacloud}};
\node[text=black,right] (R2)  at (7,4.5) {CMEMS INSTAC};
\node[text=black,right] (R3)  at (7,4.) {\ldots};

\node[text=black,right] (C1)  at (7,2) {Sextant};
\node[text=black,right] (C2)  at (7,1.5) {SOCIB data products};
\node[text=black,right] (C3)  at (7,1) {\ldots};

\node[text=black,right] (A2)  at (7,.5) {\aiOrcid~\aiResearchGate};
\node[text=black,right] (A3)  at (7,0) {\includegraphics[height=2ex]{marineID}};
\node[text=black,right] (A4)  at (7,-0.5) {\ldots};

\draw[->,>=stealth,ultra thick] (Data) edge[out=0,in=180] (Repositories);
\draw[->,>=stealth,ultra thick] (Data) edge[out=0,in=180] (Journal);
\draw[->,>=stealth,ultra thick] (Methods) edge[out=0,in=180] (Journal);

\draw[->,>=stealth,ultra thick] (Results) edge[out=0,in=180] (Journal);
\draw[->,>=stealth,ultra thick] (Results) edge[out=0,in=180] (Catalog);
%\draw[->,>=stealth,ultra thick] (Authors) edge[out=0,in=180] (Journal);
\draw[->,>=stealth,ultra thick] (Authors) edge[out=0,in=180] (Orcid);

\draw[->,>=stealth,ultra thick] (Journal) edge[out=0,in=180] (J1);
\draw[->,>=stealth,ultra thick] (Journal) edge[out=0,in=180] (J2);
\draw[->,>=stealth,ultra thick] (Journal) edge[out=0,in=180] (J3);

\draw[->,>=stealth,ultra thick] (Repositories) edge[out=0,in=180] (R1);
\draw[->,>=stealth,ultra thick] (Repositories) edge[out=0,in=180] (R2);
\draw[->,>=stealth,ultra thick] (Repositories) edge[out=0,in=180] (R3);

\draw[->,>=stealth,ultra thick] (Catalog) edge[out=0,in=180] (C1);
\draw[->,>=stealth,ultra thick] (Catalog) edge[out=0,in=180] (C2);
\draw[->,>=stealth,ultra thick] (Catalog) edge[out=0,in=180] (C3);

\draw[->,>=stealth,ultra thick] (Orcid) edge[out=0,in=180] (A2);
\draw[->,>=stealth,ultra thick] (Orcid) edge[out=0,in=180] (A3);
\draw[->,>=stealth,ultra thick] (Orcid) edge[out=0,in=180] (A4);

\end{tikzpicture}
\end{figure}
\end{block}

\begin{block}{Unique identifiers for each components}
\justifying
\aiDoi~-- minted on $\left\{\begin{tabular}{l}
Datasets \\
Data products (e.g., climatologies)\\
\end{tabular}\right.$\\
\aiOrcid~-- identifies authors
\end{block}

\begin{block}{}
\begin{figure}
\begin{tikzpicture}[scale=2]
\node[fill=LightYellow,text=black] (OceanObservation)  at (1,7) {Ocean Observation};
\node[fill=LightGreen,text=black] (Science) at (9,7) {Science};
\node[fill=LightSkyBlue,text=black] (Software2) at (1,6) {Software tool};
\node[fill=LightYellow,text=black] (GenerateDataset) at (1,5) {Dataset};
\draw[->,>=stealth] (OceanObservation) edge (Software2);
\draw[->,>=stealth] (Software2) edge (GenerateDataset);
\node[fill=LightSkyBlue,text=black] (Software) at (5.5,5) {Software tool};
\node[fill=LightGreen,text=black] (Publication) at (9,5) {Publication};
\draw[->,>=stealth] (Science) edge (Publication);
\draw[->,>=stealth] (GenerateDataset) edge node[anchor=center, above] {\scriptsize Analysis} (Software);
\draw[->,>=stealth] (Software) edge node[anchor=center, above] {\scriptsize Results} (Publication);
\node[fill=LightYellow,text=black] (DOIdata) at (1,3.5) {\aiDoi};
\node[fill=LightGreen,text=black] (DOIpub) at (9,3.5) {\aiDoi};
\draw[->,>=stealth](GenerateDataset) edge (DOIdata);
\draw[->,>=stealth](Publication) edge (DOIpub);
\end{tikzpicture}
\end{figure}
\end{block}


\begin{block}{Is this sufficient to go from data to results~\faQuestionCircle}

\begin{figure}
\includegraphics[width=.6\textwidth]{paperread}\\
{\scriptsize\textbf{Source:} "\textit{English Communication for Scientists}", Nature, \url{https://www.nature.com/scitable/ebooks/english-communication-for-scientists-14053993/writing-scientific-papers-14239285}}
\end{figure}
\vspace{1.5cm}

The software tools should also be properly versioned and cited to document:
\begin{enumerate}
\item how the observations are converted to a dataset,
\item how scientific results are derived from the dataset.
\end{enumerate}
\end{block}

\begin{block}{Making the code available}
\justifying
If the code of the software tool(s) is open, there are several ways to distribute it:
\begin{figure}
\begin{tikzpicture}[scale=2]
\node[text=black,left,text width=3cm] (Code)  at (0,1.5) {Software code};
\node[text=black,right,text width=3.5cm] (Journals)  at (1,3) {Journals};
\node[text=black,right,text width=3.5cm] (Platforms)  at (1,1.5) {Platforms};
\node[text=black,right,text width=3.5cm] (Hosting)  at (1,0) {Hosting services};

\node[text=black,right] (J1)  at (5,4.) {Geoscientific Model Development};
\node[text=black,right] (J2)  at (5,3.5) {Earth Science Informatics};
\node[text=black,right] (J3)  at (5,3.) {Methods in Oceanography};
\node[text=black,right] (J4)  at (5,2.5) {\ldots};

\node[text=black,right] (P1)  at (5,2) {\includegraphics[height=.8cm]{logo_figshare}~\includegraphics[height=.8cm]{logo_dspace}};
\node[text=black,right] (P2)  at (5,1.5) {\includegraphics[height=.8cm]{logo_ckan}~\includegraphics[height=.8cm]{logo_zenodo}};
\node[text=black,right] (P3)  at (5,1.) {\ldots};

\node[text=black,right] (H1)  at (5,0.5) {\includegraphics[height=.8cm]{logo_bitbucket.png}~~\includegraphics[height=.8cm]{Octocat.png}};
\node[text=black,right] (H2)  at (5,0) {\includegraphics[height=.8cm]{logo_sourceforge}~~\includegraphics[height=.8cm]{logo_gitlab}~~\includegraphics[height=1cm]{logo_coding}};
\node[text=black,right] (H3)  at (5,-0.5) {\ldots};

\draw[->,>=stealth,ultra thick] (Code) edge[out=0,in=180] (Journals) edge[out=0,in=180] (Platforms) edge[out=0,in=180] (Hosting);
\draw[->,>=stealth,ultra thick] (Journals) edge[out=0,in=180] (J1) edge[out=0,in=180] (J2) edge[out=0,in=180] (J3) edge[out=0,in=180] (J4);
\draw[->,>=stealth,ultra thick] (Platforms) edge[out=0,in=180] (P1) edge[out=0,in=180] (P2) edge[out=0,in=180] (P3);
\draw[->,>=stealth,ultra thick] (Hosting) edge[out=0,in=180] (H1) edge[out=0,in=180] (H2) edge[out=0,in=180] (H3);
\end{tikzpicture}
\end{figure}
\end{block}

\end{column}

% 2nd column
%-------------------------------------------------------------------------------

\begin{column}{.31\linewidth}

\begin{block}{Research platforms}
\begin{tikzpicture}[remember picture,overlay]
\node [scale=2.5,text opacity=0.25,color=Grey]
at (10,-1) {{\Large~~~\faFilePdfO~~~\faDatabase~~~\faImage~~~\faCode}};
\end{tikzpicture}
Online infrastructures whose objective is to persistently store and archive digital artifacts relevant to research: articles, data, images, code, model outputs, \ldots\\
Comparison \faArrowCircleDown.


\begin{tikzpicture}[remember picture,overlay]
\node [scale=6,text opacity=0.2,color=Grey]
at (14,-8) {Comparison};
\end{tikzpicture}

\begin{table}
%\footnotesize
\begin{tabularx}{\textwidth}{@{}c P P P P @{}}
\rowcolor{LightYellow} Tool 		& 	CKAN		& DSpace		& 	Figshare 	& Zenodo	\\
Open Source 						&  \yestab		& \yestab		& No			& \yestab	\\
%\faGithub							& ckan/ckan 	& DSpace/DSpace & -- 			& zenodo/zenodo	\\
Licence								& Affero GNU GPL v3.0 & BSD 	& -- 			& GPL-2.0		\\
1st released						& November 2011	& November 2002 & January 2011 	& May 2013 		\\
Language 					& Python		& Java 			& --			& Python		\\
Deployment							& Local			& Local			& Cloud			& Cloud			\\
\faPlug~with \faGithub 			& No	 		& No			& \yestab 		& \yestab		\\
\faPlug~with \aiOrcid			& \yestab	 	& Not direct 	& \yestab 		& Login			\\
\end{tabularx}
\end{table}
\end{block}

\begin{block}{Let's have a closer look at Zenodo}

\begin{itemize}
\item[\Checkmark] ingest all research outputs and any file format
\item[\Checkmark] DOIs assigned to have uniquely citable files
\item[\Checkmark] integrated into reporting lines for research via \href{https://www.openaire.eu/}{OpenAIRE}.
\end{itemize}

\begin{figure}
\begin{tikzpicture}
\node[anchor=south west,inner sep=0cm] (image) at (0,0) {\includegraphics[width=0.85\textwidth]{./figures/zenodo1.png}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]

\draw[dashed,-stealth,thick] (.57,.55) to[out=0, in=180] (0.76,0.72) node[right] {\footnotesize GitHub};
\draw[dashed,-stealth,thick] (.57,.49) to[out=0, in=180] (0.76,0.65) node[right] {\footnotesize ORCID};
\draw[dashed,-stealth,thick] (.58,.08) to[out=0, in=180] (0.76,0.58) node[right] {\footnotesize Create account};    
\end{scope}
\end{tikzpicture}

\vspace{1cm}

\begin{tikzpicture}
\node[anchor=south west,inner sep=0cm] (image) at (0,0) {\includegraphics[width=0.85\textwidth,frame]{./figures/zenodo_linked.png}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
%        \draw[help lines,xstep=.1,ystep=.1] (0,0) grid (1,1);
%        \foreach \x in {0,1,...,9} { \node [anchor=north] at (\x/10,0) {0.\x}; }
%        \foreach \y in {0,1,...,9} { \node [anchor=east] at (0,\y/10) {0.\y}; }  
        \draw[ultra thick,draw=DarkOrange] (.65,.4) rectangle (.25,0.55);
\end{scope}
\end{tikzpicture}
\vspace{1cm}

\includegraphics[width=0.75\textwidth,frame]{./figures/zenodo_repos.png}
\caption{Zenodo log in: using other accounts is great feature. When linked with GitHub, Zenodo creates a DOI every time one makes a release in one of the enabled repositories.}
\end{figure}

We now have all the pieces to cite the code used in the research:
\vspace{1cm}

\begin{enumerate}
\item Zenodo login using \faGithub~or~\aiOrcid
\item Upload of software code to \faGithub~or to Zenodo
\item Generation of the \aiDoi~for a given version of the code
\end{enumerate}

\vspace{1cm}

\begin{figure}
\centering
\begin{tikzpicture}[scale=2]
\node[fill=LightSkyBlue,text=black] (Software) at (5.5,5) {Software tool};
\node[fill=DarkOrange,text=black] (GitHub) at (4.5,.5) {\faGithub};
\node[fill=LightSkyBlue,text=black] (SourceCode) at (8,2) {Source code};
\draw[->,>=stealth](SourceCode) edge[in=0,out=-90] node[anchor=center, above, sloped] {\scriptsize Upload} (GitHub);
\draw[->,>=stealth](Software) edge[in=90,out=0] (SourceCode);
\node[fill=DarkOrange,text=black] (ORCID) at (1,.5) {\aiOrcid};
\node[fill=LightSkyBlue,text=black] (Zenodo) at (4.5,2) {\includegraphics[height=.4cm]{figures/zenodo-black-200.png}}; 
\draw node[vertex] (Joint) at (2.5,0.5) {};
\draw[->,>=stealth](Software)(Joint) edge node[anchor=center, above,sloped] {\scriptsize Login} (Zenodo);
\draw[-] (ORCID) to (Joint);
\draw[-] (GitHub) to (Joint);
\node[fill=LightSkyBlue,text=black] (DOIcode) at (4.5,3.5) {\aiDoi};
\path[->,>=stealth]
(Software)(Zenodo) edge (DOIcode)
(SourceCode) edge node[anchor=center, above] {\scriptsize Upload} (Zenodo);
\path[->,>=stealth,line width=1.5pt];
\end{tikzpicture}
\caption{Getting unique identifiers for software codes using Zenodo.}
\end{figure}

\end{block}

\begin{block}{Example 1}

\parbox{.5\textwidth}{
From observations to dataset.

The SOCIB glider toolbox is a set of MATLAB/Octave scripts to manage data collected by a glider fleet: data download, processing and figure generations. 
}\parbox{.5\textwidth}{
\begin{figure}
\centering
\includegraphics[width=.45\textwidth]{SOCIB_Canales_Oct2014_salinity.png}
\caption{Example of a glider section produced by the toolbox.}
\end{figure}
}

The code development was carried out in GitHub (\url{https://github.com/socib/glider_toolbox}, credits to T.~Garau and J.P. Beltran) and was recently coupled to Zenodo.

\includegraphics[height=1.25cm]{glider_zenodo.pdf}

\end{block} 

\end{column}


% 3rd column
%-------------------------------------------------------------------------------

\begin{column}{.31\linewidth}

\begin{block}{Example 2: the DIVA interpolation tool}

\begin{tikzpicture}[remember picture,overlay]
\node [scale=7,text opacity=0.2,color=Grey]
at (14,-5) {History};
\end{tikzpicture}
\begin{description}
\item[1990’s:] Variational Interpolation Method (Fortran 77) only 2D interpolations
\item[2006] SeaDataNet, code refactory and set of bash scripts 
\item[2007] \faPlug~with ODV \includegraphics[height=1.5ex]{odv_logo_small}
\item[2008] code in Subversion \includegraphics[height=2ex]{logo_svn}, distribution through GHER web page
\item[2009] new modules in Fortran 90 for loops over depth and time 
\item[2012] new error calculation technique
\item[2017] new version control system:
\begin{enumerate}
\item Switch from SVN to \faGit, distribution via \faGithub, sync with \includegraphics[height=2ex]{zenodo-black-200}
\item Enable Diva repository in Zenodo
\item Edit the different \textit{tags} on GitHub to get DOI
\end{enumerate}
\end{description}

\begin{figure}
\begin{tikzpicture}
\node[anchor=south west,inner sep=0cm] (image) at (0,0) {\includegraphics[width=0.9\textwidth,frame]{./figures/zenodo_diva_rel.png}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
%        \draw[help lines,xstep=.1,ystep=.1] (0,0) grid (1,1);
%        \foreach \x in {0,1,...,9} { \node [anchor=north] at (\x/10,0) {0.\x}; }
%        \foreach \y in {0,1,...,9} { \node [anchor=east] at (0,\y/10) {0.\y}; }  
\node[text=DarkOrange] (ORCID) at (0.4,0.82) {\footnotesize ORCID};
\draw[dashed,-stealth,thick, DarkOrange] (.1,.68) to[out=90, in=180] (ORCID);
\draw[dashed,-stealth,thick, DarkOrange] (.21,.68) to[out=90, in=-135] (ORCID);
\draw[dashed,-stealth,thick, DarkOrange] (.33,.68) to[out=90, in=-90] (ORCID);
\draw[dashed,-stealth,thick, DarkOrange] (.86,.18) to[out=0, in=-90]  (.95,.25) node[above] {\footnotesize DOI};

\end{scope}
\end{tikzpicture}
\caption{\faArrowCircleUp~Main page of DIVA software in Zenodo. Note the ORCID logo with the authors and the DOI relative to the code. \faArrowCircleDown~Share on social media and "\textit{cite as}" options.}
\includegraphics[width=.25\textwidth]{zenodo_citeas}
\end{figure}
\end{block}

\begin{block}{Putting all the pieces together}

To ensure \important{reproducibility} and \important{traceability}, unique identifiers (\aiDoi) are attributed to:
\begin{itemize}
\item Datasets
\item Software tools
\item Authors
\item Scientific results
\end{itemize} 
Ideally, all the identifiers should be present in the published version of the research paper.

\begin{figure}
\begin{tikzpicture}[scale=2.5]
\node[fill=LightYellow,text=black] (OceanObservation)  at (1,7) {Ocean Observation};
\node[fill=LightGreen,text=black] (Science) at (9,7) {Science};
\node[fill=LightSkyBlue,text=black] (Software2) at (1,6) {Software tool};
\node[fill=LightYellow,text=black] (GenerateDataset) at (1,5) {Dataset};
\draw[->,>=stealth] (OceanObservation) edge (Software2);
\draw[->,>=stealth] (Software2) edge (GenerateDataset);
\node[fill=LightSkyBlue,text=black] (Software) at (5.5,5) {Software tool};
\node[fill=LightGreen,text=black] (Publication) at (9,5) {Publication};
\draw[->,>=stealth] (Science) edge (Publication);
\draw[->,>=stealth] (GenerateDataset) edge node[anchor=center, above] {\scriptsize Analysis} (Software);
\draw[->,>=stealth] (Software) edge node[anchor=center, above] {\scriptsize Results} (Publication);
\node[fill=LightYellow,text=black] (DOIdata) at (1,3.5) {\aiDoi};
\node[fill=LightGreen,text=black] (DOIpub) at (9,3.5) {\aiDoi};
\draw[->,>=stealth](GenerateDataset) edge (DOIdata);
\draw[->,>=stealth](Publication) edge (DOIpub);
\node[fill=DarkOrange,text=black] (GitHub) at (4.5,.5) {\faGithub};
\node[fill=LightSkyBlue,text=black] (SourceCode) at (8,2) {Source code, notebook};
\draw[->,>=stealth](SourceCode) edge[in=0,out=-90] node[anchor=center, above, sloped] {\scriptsize Upload} (GitHub);
\draw[->,>=stealth](Software) edge[in=90,out=0] (SourceCode);
\node[fill=DarkOrange,text=black] (ORCID) at (1,.5) {\aiOrcid};
\node[fill=LightSkyBlue,text=black] (Zenodo) at (4.5,2) {\includegraphics[height=.4cm]{figures/zenodo-black-200.png}}; 
\draw node[vertex] (Joint) at (2.5,0.5) {};
\draw[->,>=stealth](Software)(Joint) edge node[anchor=center, above,sloped] {\scriptsize Login} (Zenodo);
\draw[-] (ORCID) to (Joint);
\draw[-] (GitHub) to (Joint);
\node[fill=LightSkyBlue,text=black] (DOIcode) at (4.5,3.5) {\aiDoi};
\path[->,>=stealth]
(Software)(Zenodo) edge (DOIcode)
(SourceCode) edge node[anchor=center, above] {\scriptsize Upload} (Zenodo);
\path[->,>=stealth,line width=1.5pt]
(DOIdata) edge[in=180,out=0] (Publication)
(DOIcode) edge[in=180,out=0] (Publication);
\end{tikzpicture}
\caption{From data to final results: all the components are identified and citable.}
\end{figure}


\end{block}

\begin{block}{Acknowledgements}
\justifying
The work presented in this poster has been developed in the frame of\\
\important{SeaDataCloud}--\textit{Further developing the pan-European infrastructure for marine and ocean data management}, Project ID~730960 and\\
\important{ODIP~2}--\textit{Extending the Ocean Data Interoperability Platform}, Project ID: 654310.
\vspace{.5cm}

\begin{center}
\includegraphics[height=2.5cm]{logo_odip}\hspace{1cm}\includegraphics[height=2.5cm]{logo_seadatacloud}
\end{center}

\end{block}


\end{column}

\end{columns}

\vfill

\end{frame}

\end{document}





