\input{phidias_style.tex}
\newcolumntype{x}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}p{#1}}
\newcolumntype{y}[1]{>{\let\newline\\\arraybackslash\hspace{0pt}}p{#1}}
\setlength{\parskip}{2mm} 

\usepackage{smartdiagram}
\usepackage{metalogo}

\definecolor{blueocean}{HTML}{B7E2F0}
\definecolor{greenwood}{HTML}{B4F1AE}
\definecolor{yellowor}{HTML}{F1E0AE}



\title{WP6 | Ocean use case | Spatial interpolation}
\author[C.~Troupin]{C.~Troupin, A.~Barth, J.-M.~Beckers}
\institute{GHER~-~ULiège}
\date{27-28 October, 2021 | PHIDIAS Plenary meeting, Pisa (Italy)}
\titlegraphic{\includegraphics[height=\logoheight]{logo_uliege_transp}~\includegraphics[height=\logoheight]{logo_gher_transp}
}

\begin{document}
{\date{}
\usebackgroundtemplate{\includegraphics[width=\paperwidth]{phidias_back1.png}}
\begin{frame}
\vspace{3cm}
\maketitle
\end{frame}
}

\usebackgroundtemplate{\includegraphics[width=\paperwidth]{phidias_back2.png}}

\begin{frame}
\frametitle{Who are we?}

\begin{columns}
\begin{column}{0.55\textwidth}
GeoHydrodynamics and Environment Research (GHER)

\begin{itemize}
\footnotesize
\item[3] senior researchers
\item[2] postdoctoral researchers
\item[4] PhD students
\end{itemize}

Organizers of the International Liège Colloquium on Ocean Dynamics\\ 
{\footnotesize(53rd edition in 2022)}


\end{column}

\begin{column}{0.45\textwidth}  %%<--- here
    \begin{center}
     \includegraphics[width=0.9\textwidth]{th-1024x0-nihoul_jacques.jpg}\\
     \includegraphics[width=0.9\textwidth]{Calvi2017_9369.jpg}
     \end{center}
\end{column}
\end{columns}



\end{frame}

\begin{frame}[b]
\frametitle{What do we do?}
\parbox{.6\textwidth}{
\begin{center}
\footnotesize
\smartdiagram[bubble diagram]{Ocean,
  Data analysis\\ and assimilation, Software \\development\\ (spatial \\interpolation), Numerical\\ modelling}
\end{center}
}\parbox{.4\textwidth}{
\onslide*<2->{
(Almost) always in open source:\\ 
\url{https://github.com/gher-ulg/}

\vspace{.5cm}

(Almost) always published in peer-reviewed journals
}
}
\end{frame}

\begin{frame}{Ocean observation is complex and costly}
\parbox{.6\textwidth}{
\includegraphics<1>[width=.5\textwidth]{EMODnet_data_domains_phosphate.png}
}\parbox{.4\textwidth}{
1 day of ship time = 10000$-$40000€\\
(compare with data archiving\ldots)
\vspace{1cm}
1 glider = 50000$-$250000€\\
(same order of magnitude for maintenance)
}
\end{frame}

\begin{frame}
\Wider{
\includegraphics<1>[width=\textwidth]{2021-10-10-00 00_2021-10-10-23 59_Sentinel-2_L1C_True_color.jpg}
\includegraphics<2>[width=\textwidth]{2021-10-20-00 00_2021-10-20-23 59_Sentinel-2_L1C_True_color.jpg}
}
\end{frame}

\begin{frame}
\frametitle{\faWrench~~Our main tools}

\begin{description}[leftmargin=!]
\item[DIVA] spatial interpolation in 2 dimensions \comment{Fortran }
\item[DIVAnd] spatial interpolation in $n$ dimensins \comment{MATLAB $\rightarrow$ Julia} 
\item[\textcolor{titlecolor}{DINEOF}] reconstruction of clouds or gaps in satellite images \comment{Fortran}
\item[\textcolor{titlecolor}{DINCAE}] reconstruction of clouds using neural network \comment{Python $\rightarrow$ Julia}
\end{description}
\vspace{1cm}
\onslide<2->{Comments:
\begin{enumerate}
\footnotesize
\item \textcolor{titlecolor}{DINEOF}/\textcolor{titlecolor}{DINCAE} (GPU!) candidates for PHIDIAS2?\\
\item Julia is a game-change for data analysis\\
\item Need to be more creative for our tools name
\end{enumerate}

}

\end{frame}


\begin{frame}[t]
\frametitle{DIVAnd deployment}

\begin{enumerate}
\item<1-> From source
\item<2-> JupyterHub with a Docker container
\item<3-> Singularity container
\end{enumerate}

\onslide*<1>{
\begin{itemize}
\footnotesize
\item Install Julia (currently at version 1.6.3): \url{https://julialang.org/downloads/}
\item Install DIVAnd + other modules: \url{https://github.com/gher-ulg/Divand.jl}
\item[]
\item[\faCheck] fast, straightforward installation
\item[\faCheck] Code can use libraries tuned for specific hardware (like BLAS, MPI,...) and some new CPU instructions 
\item[]
\item[\textcolor{red}{\faClose}] no graphical interface
\end{itemize}
}



\onslide*<2>{
\begin{itemize}
\footnotesize
\item Container from \url{https://github.com/gher-ulg/DIVAnd-jupyterhub}
\item[]
\item[\faCheck] Deployed the frame of the SeaDataCloud \textit{ Virtual Research Environment}
\item[\faCheck] Docker hub simplifies distributions + management of containers
\item[]
\item[\textcolor{red}{\faClose}] Need to manage the user identification in Jupyterhub
\item[\textcolor{red}{\faClose}] Jupyterhub not suited for long-running tasks
\item[\textcolor{red}{\faClose}] Code cannot use libraries tuned for specific hardware
\end{itemize}
}


\onslide*<3>{
\begin{itemize}
\footnotesize
\item Container from \url{https://github.com/gher-ulg/DIVAnd-singularity }
\item[]
\item[\faCheck] Simplicity, popular for HPC
\item[\faCheck] Easily integrated with job schedulers (like SLURM or PBS)?
\item[\faClose] Container files (*.sif) have to be moved manually
\item[]
\item[\textcolor{red}{\faClose}] Code cannot use libraries tuned for specific hardware 
\item[\textcolor{red}{\faClose}] Building is slow compared to Docker as there is no caching mechanism
\item[\textcolor{red}{\faClose}] Linux distributions (like Ubuntu and Debian) contains often outdated versions of Singularity
\end{itemize}
}

\end{frame}

%
\begin{frame}
\frametitle{Recent progresses}

\begin{itemize}
\item[\faCheckSquareO] Creation of a Singularity container\\ 
Julia + DIVAnd + dependencies 
\item[\faCheckSquareO] Improve DIVAnd processing with very large datasets
\item[\faCheckSquareO] Improve large data reading
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Singularity container}

\faGithubSquare~\url{https://github.com/gher-ulg/DIVAnd-singularity}

{\footnotesize
Julia V1.6.2\\
Singularity V3.7.4
}

How to install?

\begin{enumerate}
\footnotesize
\item Download the \texttt{.sif} file (549 MB) \comment{(build using GitHub actions)}
\item Run \texttt{singularity run DIVAnd.sif}
\end{enumerate}

Deployments:
\begin{enumerate}
\footnotesize
\item Personal laptops
\item CSC machine \comment{\texttt{hpc.4.20core}: 20 cores and 86 GB RAM}
\item CÉCI (Consortium des Équipements de Calcul Intensif)
\end{enumerate}
\end{frame}

\begin{frame}
\frametitle{About Singularity\ldots}

Now two main repositories:
\begin{itemize}
\item[] \url{https://github.com/hpcng/singularity}
\item[] \url{https://github.com/sylabs/singularity}
\end{itemize}

Explained in this post: \url{https://singularity.hpcng.org/news/community-update-20210529}

\parbox{.7\textwidth}{
\textit{"Sylabs has forked the Singularity repository creating their own commercially controlled repository and project. Please join us in thanking Sylabs"}
}
\end{frame}

\begin{frame}
\frametitle{Large use case}

\begin{columns}
\begin{column}{0.45\textwidth}
    \begin{center}
     \includegraphics[width=0.9\textwidth]{emodnet_oxy.png}
     \end{center}
\end{column}

\begin{column}{0.55\textwidth}
Interpolation of eutrophication variables \\
(phosphate, silicates, nitrates, \ldots)
\vspace{.5cm}
Up to 20 million data points 
\vspace{.5cm}

Grid size: 237 lon $\times$ 461 lat $\times$ 109 depth 


\end{column}
\end{columns}

\end{frame}


\begin{frame}
\frametitle{Still to address} 

\begin{itemize}

\item[] Marine data processing workflows for \textbf{on-demand} processing
\item[] Speeding up interpolation \comment{New solver being tested}
\item[] Accessing the CDI datalake \comment{See Peter's presentation}
\end{itemize}

\vspace*{1cm}
\onslide*<2>{
\begin{center}
\it Before: Trying to bring data closer to the users\\
Now: Bringing the tools closer to the users
\end{center}
}

\end{frame}

\end{document}



