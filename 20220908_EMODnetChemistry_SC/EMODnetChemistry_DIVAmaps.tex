\input{EMODnet_style2022.tex}
\usepackage[export]{adjustbox}

\subtitle{EMODnet Chemistry V, 3rd Steering Committee}
\title{DIVA maps | Transfer to Central Portal}
\author[C.~Troupin]{C.~Troupin, A.~Barth \& EMODnet Chemistry team}
\institute{GHER~-~ULiège}
\date{8-9 September, 2022}
\titlegraphic{\includegraphics[height=\logoheight]{logo_uliege}~\includegraphics[height=\logoheight]{logo_gher}
}

\definecolor{highlightext}{HTML}{8E8E8E}

\urlstyle{rm}

\begin{document}

{
\usebackgroundtemplate{\includegraphics[width=\paperwidth]{background1.jpg}}

\begin{frame}[plain]

\begin{tikzpicture}[remember picture, overlay]
\draw (current page.south west) +(1.4, 5.25) node[anchor=south west] {\usebeamerfont{title}\usebeamercolor[fg]{title}\inserttitle};
\draw (current page.south west) +(1.4, 4.) node[anchor=south west,text width=.9\textwidth,align=left] {\usebeamerfont{subtitle}\usebeamercolor[fg]{subtitle}\insertsubtitle\newline \usebeamerfont{date}\usebeamercolor[fg]{date}\insertdate};

\draw (current page.south west) +(9., 2.75) node[anchor=north east,text width=7.5cm,align=right] {\usebeamerfont{author}\usebeamercolor[fg]{author}\insertauthor};

\draw (current page.south west) +(9.1, 2.75) node[anchor=north west,text width=2.75cm,align=left] {\usebeamerfont{author}\usebeamercolor[fg]{author}\insertinstitute};

\draw (current page.south west) +(9.1, 1.75) node[anchor=north west,align=left] {\usebeamerfont{author}\usebeamercolor[fg]{author}\inserttitlegraphic};

\end{tikzpicture}
\end{frame}
}


\usebackgroundtemplate{\includegraphics[width=\paperwidth]{background2.jpg}}

\begin{frame}
\frametitle{Topics of the day}

\begin{tabular}{C{.3\textwidth}C{.3\textwidth}C{.3\textwidth}}
\huge 1 & \huge 2 & \huge 3 \\
DIVAnd and NCDatasets: what's new? & Preparation of the merged products for the EMODnet Central Portal (CP) & Timeline for the preparation of the new products\\
\end{tabular}

\end{frame}

\begin{frame}
\begin{table}
\footnotesize
\begin{tabular}{llc}
\toprule
Deliverable & Topic		& Delivery						\\
\midrule
\textbf{D3.5} & High-resolution DIVA maps near river mouths & M16 \\
\textbf{D3.7} & New pan-European DIVA maps for eutrophication &	M14, M23\\
\bottomrule
\end{tabular}
\end{table}
\end{frame}


\begin{frame}[c]
\frametitle{}
\centering
\fontsize{30}{30}\selectfont
DIVAnd and NCDatasets:\\ 
what's new?

\end{frame}

\begin{frame}[b]
\frametitle{New release of Julia}

Version \textbf{1.8.0} released on August 17, 2022 \\
(\faLink~\url{https://docs.julialang.org/en/v1/NEWS/})

\begin{figure}
\includegraphics[width=.7\textwidth]{julia18}
\end{figure}

\begin{itemize}
\footnotesize
\item Current stable release
\item Improved package manager
\item Please install that version!
\end{itemize}
\end{frame}

\begin{frame}[c]
\frametitle{New developments in DIVAnd}

\onslide<2>{
\begin{figure}
\includegraphics[width=2cm]{logo_divand}
\end{figure}
}

Version \textbf{2.7.9} released on August 23, 2022~\includegraphics[height=0.3cm]{zenodo.7016823.png}

\begin{itemize}
\item Solved bulls eye in region without data 
\item Document how to leverage other type of data (satellite)
\item Fixing compatibility issues with other packages
\onslide<2>{\item (and we have now a logo)}
\end{itemize}

\end{frame}

\begin{frame}[b]
\frametitle{DIVAnd deployment: using Singularity container}

Version \textbf{1.0.0} released on August 19, 2022

\begin{figure}
\centering
\fbox{\includegraphics[width=.8\textwidth]{singularity_container}}
\end{figure}

\begin{itemize}
\item Installation is straightforward
\item Suitable to deploy in \textbf{HPC} environments
\item[\faEnvelope] Contact us if you're interested to use it
\end{itemize}

\end{frame}

\begin{frame}[c]
\frametitle{New developments in NCDatasets}

Version \textbf{0.12.17} released on August 21, 2022 

\faGithub~\url{https://github.com/Alexander-Barth/NCDatasets.jl}

\begin{itemize}
\item Use netCDF-C v4.9.0 
\item Time dimension with month units can now be loaded
\item Using \texttt{ncgen} with variable names with spaces\\
(generate code to create a netCDF file with the same metadata as a template file)
\end{itemize}

\end{frame}

\begin{frame}[c]
\frametitle{}
\centering
\fontsize{30}{30}\selectfont
Merged products\\ 
for the Central Portal

\end{frame}

\begin{frame}[c]
\frametitle{Adopting a merging strategy}

\textbf{Goal:} decrease the number of layers

Merging by:
\begin{itemize}
\item[\textcolor{red}{\faTimes}] Regions: too different (resolution, depth levels etc)
\item[\textcolor{red}{\faTimes}] Variables: would lead to huge files
\item[\textcolor{DarkGreen}{\faCheck}] Seasons: feasible

\end{itemize}

\end{frame}

\begin{frame}[c]
\frametitle{Merging seasons for the regional seas}
\begin{itemize}
\footnotesize
\item Divide the number of layers for the regional seas by \textbf{4}
\item Increase file size by 4 (approx.)\\
(up to \textbf{20G} per file for the Atlantic region)
\item Need to take care of some details\\ 
(variable storing the observation IDs)
\item \textbf{6} regions, \textbf{5} variables, large files\ldots
\end{itemize}

\end{frame}

\begin{frame}[c]
\frametitle{The process: issues and solutions}
\vspace{.5cm}

\begin{itemize}
\item \textbf{Issue 1:} not enough disk space on CINECA server\\
Solution 1: Quickly solved by CINECA staff
\item \textbf{Issue 2:} (probably) not enough RAM\\
Solution 2: Run on local machine at ULiège
\item \textbf{Issue 3:} file upload too slow\\
Solution 3: Keep CINECA machine and change strategy for merging
\item \textbf{Issue 4:} not same number of depth levels in Black Sea\\
Solution 4: Create new script to add (empty) missing levels
\item \textbf{Issue 5:} time units in Atlantic Ocean\\
Solution 5: linked to \texttt{udunits} packages\ldots
\end{itemize}

\end{frame}

\begin{frame}[c]
\frametitle{New strategy}

Use only \texttt{nco} tools (netCDF operators), instead of \texttt{nco} + Julia

\begin{enumerate}
\item Split each seasonal file into individual file\\
(one file per time period)
\item Merge all the individual files into a global file\\
\item Edit attributes 
\item Add the observations IDs
\end{enumerate}

\end{frame}

\begin{frame}[t]
\frametitle{Current status}

\vspace{1cm}

\faCheckSquareO = done\hspace{1cm}\faGears = work in progress\hspace{1cm}\faHourglassStart = not yet started


\begin{itemize}
\item[\faCheckSquareO] Meeting with CP about the merging \hfill{1 June, 2022}
\item[\faCheckSquareO] Generate merged files
\item[\faCheckSquareO] Put all the observations into the merged files
\item[\faCheckSquareO] Progress meeting  \hfill{12 July, 2022}
\item[\faCheckSquareO] Edit the global attributes \hfill{12 July, 2022}
\item[\faCheckSquareO] Transfer to Central Portal \hfill{13 July, 2022}
\item[\faGears] Edit the WMS links in Sextant
\item[\faHourglassStart] (Re)move seasonal files
\end{itemize}
\end{frame}

\begin{frame}[c]
\centering
\includegraphics[width=1.1\textwidth]{oceanbrowser_now.png}
\end{frame}

\begin{frame}
\frametitle{What about OceanBrowser?}

\begin{itemize}
\item Still hosted on CINECA server
\item Time resources for maintenance not negligible
\item Initially thought it would be discontinued\\
(visualisation already done by CP viewer)
\item Then request to maintain it as a back-end service 
(handling \textbf{WMS} requests from CP viewer), without users actually seeing it
\item Much more flexible and well-adapted to EMODnet Chemistry products than ERDDAP
\end{itemize}


\end{frame}

\begin{frame}
\frametitle{Conclusions}

\begin{enumerate}
\item Nothing difficult, but nothing trivial
\item Lot of manual operations and specific code
\item Many (many) limitations in ERDDAP 
\item Modifications to be taken into account\\ 
for the next product release
\end{enumerate}
\end{frame}


\begin{frame}[c]
\frametitle{}
\fontsize{30}{30}\selectfont
\centering
Preparation\\ of the new products
\end{frame}


\begin{frame}[t]
\frametitle{Proposed workflow}
\vspace{.5cm}

\begin{enumerate}
\item<1-> Upgrade of Julia (1.8.0) and DIVAnd (2.7.9)
\item<2-> Preparation of the data files (ODV netCDF or contiguous ragged array representation)
\item<3-> Setting of domain extension, spatial resolution, time periods and depth levels
\item<4-> Create the land-sea mask using bathymetry (GEBCO for regional products, EMODnet Bathymetry for coastal products)
\item<5-> Setting of the background field 
\item<6-> Preparation of metadata (project, EDMO code, authors, \ldots)
\item<7-> Analysis and plots 
\end{enumerate}

\end{frame}

\begin{frame}[c]
\frametitle{Lessons from previous phase}

\begin{enumerate}
\item<1-> Have the \textbf{data ready} as soon as possible
\item<2-> \textbf{Contact us} at anytime (\texttt{A.barth@uliege.be} and \texttt{ctroupin@uliege.be}) or via \faGithub~issues!
\item<3-> Make sure the \textbf{quality} of the observations is good
\item<4-> Start \textbf{easy} (low resolution, simple background, constant analysis parameters) to get something working quickly
\item<5-> \textbf{Metadata} can take a while\ldots
\item<6-> No more separation by seasons in the files\\
(but still visible in the time filtering)
\end{enumerate}

\end{frame}

\begin{frame}[c]
\frametitle{}
\centering

\huge Let's do it!

\end{frame}


\end{document}


