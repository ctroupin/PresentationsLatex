# PresentationsLatex

This project aims to store the source files (LaTeX) used to generate the conference presentations and posters.    
The pdf will be stored and images won't be stored in GitHub due to memory issues.

## Turning powerpoint template into LaTeX

### The context
Sometimes you are asked to use THE powerpoint template for a presentation. Frequently the template is not too well designed: slides overload with information and logo, strange color combinations, whatever, it's not the topic of this post. You just must use it.

But, as you're more used to work with LaTeX, the task of using the template, at least the first time, can be tedious.


### The steps to follow

1. Get the template
2. Use a screenshot tool to get the background or print the ppt to pdf (lossless); `dpi` doesn't change anything in the file size
3. Get the good fonts: `xelatex` might be a good idea, also keep in mind that your template might use commercial fonts
4. Correctly place the slide title, sometimes it needs some horizontal and vertical shift in order not to overlay the logos (or other decoration items)
5. Prepare the title slide: ensure the font size and the different elements are correctly placed. 
6. To be continued...
