\documentclass[final,svgnames]{beamer}
\usepackage{tcolorbox}
\usepackage{pythonhighlight}
\newcommand{\minispace}{\hspace{.025\textwidth}}

\mode<presentation>
{
  \usetheme{GHERposter}
}


\hypersetup{bookmarksopen=true,
bookmarksnumbered=true,  
pdffitwindow=false, 
pdfstartview=FitH,
pdftoolbar=true,
pdfmenubar=true,
pdfwindowui=true,
pdfsubject={EGU2018},
pdfkeywords={Diva, SeaDataCloud, ODIP, Python},
bookmarksopenlevel=1,
colorlinks=true,urlcolor=bluegher,
citecolor=bluegher}

\graphicspath{
{../../figures4presentation/logo/},
{./figures/}
}

% Uncomment next line to display a grid to help align images
%\beamertemplategridbackground[1cm]

\title[pydiva2d]{\texttt{pydiva2d}: a python interface to the DIVA interpolation software}
\author{Charles Troupin, Alexander Barth, Sylvain Watelet and Jean-Marie Beckers}
\institute{GeoHydrodynamics and Environment Research, University of Liège}
\date{8-13 April, 2018}


\begin{document}
\begin{frame}[fragile]{} 
\centering
%\vspace{-1cm}
{\tcbset
{colframe=bluegher,nobeforeafter,colupper=white,colback=bluegher,arc=1mm,boxsep=3pt,height=1.5cm,center title}
\raisebox{8pt}{{\large \faTags}}~\tcbox{Spatial interpolation}~\tcbox{Oceanography}~\tcbox{Data analysis}~\tcbox{Finite-element method}~\tcbox{python}~\tcbox{SeaDataCloud}\hspace{1cm}\raisebox{10pt}{{\large \faWrench}}~\tcbox{Python}~\tcbox{Leaflet}
}

\rule{\columnwidth}{.2cm}
  
\begin{columns}[T]
  
% 1st column %-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\begin{column}{.3\linewidth}
    
    
\begin{block}{\secnum{1}~What is DIVA?}
\justifying
A scientific tool designed to efficiently interpolate oceanographic observations.\\
The finite-element technique employed ensures that we can work with very big data sets.\\

The code itself consists of bash scripts that call a set of Fortran executables.
\end{block}

%------------------------------------------
\begin{block}{3 reasons to use it}
\justifying
\begin{itemize}
\item[\Checkmark] Free \& open
\item[\Checkmark] Deal with huge oceanographic datasets ($>$ 1000000 points)
\item[\Checkmark] Take physical boundaries (coastline, topography) into account
\end{itemize}

\begin{table}
\caption{How to get DIVA tool?}
\centering
\begin{tabular}{|cl|}
\hline
\montant Zenodo 		& \includegraphics[height=1cm]{zenodo_diva}\\
\faGithub	& \url{https://github.com/gher-ulg/DIVA}\\
\hline
\end{tabular}
\end{table}

\end{block}

%------------------------------------------
\begin{block}{\secnum{2}~Why a python interface?}
\justifying
There are many input files to prepare prior to an analysis with DIVA, so the interface makes things easier for the new users: just select the data files and the analysis parameters, the run the code without worrying about the file names and formats.

\begin{table}
\caption{How to get the module?}
\centering
\begin{tabular}{|cl|}
\hline
\montant Zenodo 		& \includegraphics[height=1cm]{zenodo_pydiva2d}\\
\faGithub 	& \url{https://github.com/gher-ulg/DivaPythonTools}\\
\hline
\end{tabular}
\end{table}
\end{block}

%------------------------------------------
\begin{block}{What's inside the box?}
\justifying
\texttt{pydiva2d} defines \textit{classes} representing the main DIVA objects:
\begin{description}
\item[data ] consisting of geolocalised measurements,
\item[contours ] representing the physical domain (coastlines),
\item[parameters ] specifying how the interpolation is performed,
\item[mesh ] used in the solver
\item[analysis ] the result produced by the interpolation.
\end{description}

\end{block}

\begin{block}{\secnum{3}~An example in the Black Sea}
\justifying
We interpolate mixed-layer depth values obtained from in situ profiles in the Black Sea.
The different steps for the interpolation are as follows: first we load the package and define the DIVA files:

\vspace{1cm}

\begin{minipage}{.75\columnwidth}
\begin{python}
import pydiva2d
divadir = "/home/ctroupin/diva-4.7.1/"
DivaDirs = pydiva2d.DivaDirectories(divadir)
DivaFiles = pydiva2d.Diva2Dfiles(DivaDirs.diva2d)

datafile = 'MLD1.dat'
coastfile = 'coast.cont'
paramfile = 'param.par'
\end{python}
\end{minipage}
\end{block}

\begin{block}{}
\justifying

\begin{minipage}{.55\columnwidth}
\begin{figure}
\includegraphics[width=.95\columnwidth]{datapoints}
\caption{Data points representing the Mixed-layer depth (in meters). Some regions are well covered, while others have no measurements.}
\end{figure}
\end{minipage}\minispace
\begin{minipage}{.4\columnwidth}
\begin{python}
# Load parameters
param = pydiva2d.Diva2DParameters().read_from(paramfile)
# Create projection (basemap)
m = Basemap(projection='merc', llcrnrlon=param.xori, llcrnrlat=param.yori, urcrnrlon=param.xend, urcrnrlat=param.yend, resolution='i')
  
# Load data
data = pydiva2d.Diva2DData()
data.read_from(datafile)
# Make a plot
fig = plt.figure()
ax = plt.subplot(111)
m.ax = ax
dataplot = data.add_to_plot(m=m, s=10)
\end{python}
\end{minipage}

\end{block}

\end{column}

% 2nd column
%-------------------------------------------------------------------------------

\begin{column}{.3\linewidth}
  

\begin{block}{}
\justifying

\begin{minipage}{.4\columnwidth}
\begin{python}
# Load the contours from file
contour = pydiva2d.Diva2DContours()
contour.read_from(coastfile)
# Add to plot
contour.add_to_plot(m=m, linewidth=3)
\end{python}
\end{minipage}\minispace
\begin{minipage}{.55\columnwidth}
\begin{figure}
\includegraphics[width=.625\columnwidth]{contours}
\caption{Surface contours created from the bathymetry. They delimit the interpolation domain, on which the finine-element mesh will be built.\\In this application there are 28 contours, the main contour being the Black Sea.}
\end{figure}
\end{minipage}

\end{block}

\begin{block}{}
\justifying

\begin{minipage}{.55\columnwidth}
\begin{figure}
\includegraphics[width=.95\columnwidth]{mesh}
\caption{Triangular mesh used in the numerical solving of the interpolation.\\
Number of nodes: 4636.\\
Number of interfaces: 10089.\\
Number of elements: 5360.}
\end{figure}
\end{minipage}\minispace
\begin{minipage}{.4\columnwidth}
\begin{python}
# Read mesh files
mesh = pydiva2d.Diva2DMesh()
mesh = read_from(DivaFiles.mesh, DivaFiles.meshtopo)
# Get a description
mesh.describe()
# Add to plot
mesh.add_to_plot(m, linewidth=0.25, color='k')
\end{python}
\end{minipage}
\end{block}

\begin{block}{}
\justifying
\begin{minipage}{.4\columnwidth}
\begin{python}
# Perform the analysis using the specified files
results = pydiva2d.Diva2DResults().make(divadir, datafile=datafile, contourfile=coastfile, paramfile=paramfile)
# Add results to plot
resultplot = results.add_to_plot(field='analysis', m=m)
\end{python}
\end{minipage}\minispace
\begin{minipage}{.55\columnwidth}
\begin{figure}
\includegraphics[width=.95\columnwidth]{analysis}
\caption{Interpolated mixed-layer depth (in meters). The shallowest values (yellow color) are found in the north-eastern part of the domain, influenced by the Danube river runoff.}
\end{figure}
\end{minipage}
\end{block}



\begin{block}{}
\justifying
\begin{minipage}{.55\columnwidth}
\begin{figure}
\includegraphics[width=.95\columnwidth]{errorfield}
\caption{As expected, the error field displays lower values where the data coverage is higher.\\
Note the large error in the Seas of Azov and Marmara.}
\end{figure}
\end{minipage}\minispace
\begin{minipage}{.4\columnwidth}
\begin{python}
# Add the error field
errorplot = results.add_to_plot(field='error', m=m, cmap=plt.cm.hot_r)
\end{python}
\end{minipage}
\end{block}

\begin{block}{\secnum{4}~Using Leaflet library}
\justifying
Thanks to the format-conversion methods available in the \texttt{pydiva2d}, one can easily generate \important{GeoJSON} files 
(\url{http://geojson.org/}), directly ingestible in Leaflet. 
\end{block}

\end{column}


% 3rd column
%-------------------------------------------------------------------------------

\begin{column}{.24\linewidth}

\begin{block}{Leaflet?}
\justifying
\includegraphics[height=.85cm]{logo_leaflet} is a library for mobile-friendly interactive maps\\ (\url{http://leafletjs.com/}).
It comes with a bunch of plugins to create customized maps with a lot of information as layers. 
\end{block}

\begin{block}{Temperature in the MedSea}
\justifying

The following figures illustrate the DIVA input and output.

\vspace{.75cm}
\begin{minipage}{.75\columnwidth}
\begin{python}
# Write contour and mesh to geoJSON
contour.to_geojson('medsea-contours.js')
mesh.to_geojson('medsea-mesh.js')
\end{python}
\end{minipage}
\begin{figure}
\parbox{.65\columnwidth}{
\includegraphics[width=.625\columnwidth]{leaflet_poster01}
}\parbox{.35\columnwidth}{
\caption{Contours and finite-element mesh. Both have the \important{MultiPolygons} geometry.\\
Bathymetry from \url{http://www.emodnet-bathymetry.eu/}.}
}
\end{figure}

\vspace{.25cm}
\begin{minipage}{.75\columnwidth}
\begin{python}
# Write data to geoJSON
data.to_geojson('medsea-data.js')
\end{python}
\end{minipage}
\begin{figure}
\parbox{.65\columnwidth}{
\includegraphics[width=.625\columnwidth]{leaflet_poster02}
}\parbox{.35\columnwidth}{
\caption{\important{Heatmap} displaying the data density.\\
As in the Black Sea example, the distribution is heterogeneous.\\
Data source: Mediterranean Sea -- Temperature and salinity observation collection V2\\
\doi{10.12770/8c3bd19b-9687-429c-a232-48b10478581c}}
}
\end{figure}

\vspace{.25cm}
\begin{minipage}{.75\columnwidth}
\begin{python}
# Write field to geoJSON
results.to_geojson('medsea-results.js')
\end{python}
\end{minipage}
\begin{figure}
\parbox{.65\columnwidth}{
\includegraphics[width=.625\columnwidth]{leaflet_poster03}
}\parbox{.35\columnwidth}{
\caption{Analysis field of temperature.\\
The isothers are stored in a \important{FeatureCollection}, each feature being a \important{MultiPolygon} with the temperature value as a property.}
}
\end{figure}
\end{block}


\begin{block}{Summary}
\justifying
\begin{itemize}
\item[\Checkmark] DIVA is a software tool for data interpolation.
\item[\Checkmark] the module \texttt{pydiva2d} helps with:\\
the manipulation of input files, \\
the generation of figures and \\
the conversion to other formats.
\item[\Checkmark] Leaflet can be used to represented the DIVA input and outputs using \important{GeoJSON} format.
\end{itemize}
\end{block}

\begin{block}{Acknowledgements}
\justifying
The DIVA development has received funding from the European Union Sixth Framework Programme (FP6/2002--2006) under grant agreement no. 026212, \href{http://www.seadatanet.org/}{SeaDataNet}, Seventh Framework Programme (FP7/2007--2013) under grant agreement no.~283607, SeaDataNet II, SeaDataCloud and \href{http://www.emodnet.eu/}{EMODnet} (MARE/2008/03 - Lot 3 Chemistry - SI2.531432) from the \href{http://ec.europa.eu/dgs/maritimeaffairs_fisheries/index_en.htm}{Directorate-General for Maritime Affairs and Fisheries}.
\end{block}

\end{column}

\end{columns}

\vfill

\end{frame}

\end{document}





