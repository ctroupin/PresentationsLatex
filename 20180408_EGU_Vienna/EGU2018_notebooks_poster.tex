\documentclass[final,svgnames]{beamer}
\usepackage{tcolorbox}
\usepackage{cancel}
\usepackage{xparse}

\usepackage{array}
\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{R}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcommand{\yestab}{{\cellcolor{LightGreen} Yes}}

\mode<presentation>
{
  \usetheme{GHERposter}
}

\hypersetup{bookmarksopen=true,
bookmarksnumbered=true,  
pdffitwindow=false, 
pdfstartview=FitH,
pdftoolbar=true,
pdfmenubar=true,
pdfwindowui=true,
pdfauthor={Charles Troupin},
pdfsubject={EGU2018},
pdftitle={Poster qui ne veut pas s imprimer},
pdfkeywords={Diva, SeaDataCloud, ODIP, Jupyter, VRE},
bookmarksopenlevel=1,
colorlinks=true,urlcolor=bluegher,
citecolor=bluegher}

\graphicspath{
{../../figures4presentations/logo/},
{../20171002_ODIP_Galway/figures/Screenshots/},
{./figures/}
}

\renewcommand{\CancelColor}{\color{red}}
% Uncomment next line to display a grid to help align images
%\beamertemplategridbackground[1cm]

\title[Notebooks to document workflows]{Notebooks to document workflow:\\ a \xcancel{possible} \textcolor{red}{useful} component in \textcolor{black}{V}irtual \textcolor{black}{R}esearch \textcolor{black}{E}nvironments}
\author{Charles Troupin, Alexander Barth, Sylvain Watelet and Jean-Marie Beckers}
\institute{GeoHydrodynamics and Environment Research, University of Liège}
\date{8-13 April, 2018}


\begin{document}
\begin{frame}[fragile]{} 
\centering
%\vspace{-1cm}
{\tcbset
{colframe=bluegher,nobeforeafter,colupper=white,colback=bluegher,arc=1mm,boxsep=3pt,height=1.5cm,center title}
\raisebox{8pt}{{\large \faTags}}~\tcbox{Oceanography}~\tcbox{Data analysis}~\tcbox{Reproducibility}~\tcbox{SeaDataCloud}~\tcbox{ODIP}~\tcbox{Virtual Research Environments}\hspace{1cm}\raisebox{10pt}{{\large \faWrench}}~\tcbox{Python}~\tcbox{Julia}~\tcbox{Jupyter}
}

\rule{\columnwidth}{.2cm}
 
\begin{columns}[T]
  
% 1st column %-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\begin{column}{.3\linewidth}

\begin{block}{\secnum{0}~What do we want to do?}
\justifying

\begin{tikzpicture}[remember picture,overlay]
\node [scale=4,text opacity=0.1]
at (15,-2) {Reproducibility};
\end{tikzpicture}

\begin{center}
\parbox{.6\columnwidth}{
To use jupyter notebooks both as a \textbf{user guide} and as a \textbf{user interface} to describes the different steps to generate research products.
}
\end{center}
\faExternalLink~"\textit{Interactive notebooks: Sharing the code}", Nature (2014)
\url{http://www.nature.com/news/interactive-notebooks-sharing-the-code-1.16261}

\end{block}
    
    
\begin{block}{\secnum{1}~A few definitions \& acronyms\ldots}
\justifying

\begin{description}
\item[Application Programming Interface (\textbf{API}):] rules and specifications that software programs can follow to communicate with each other. 
\item[DIVAnd:] a software tool designed for the interpolation of in situ measurements in $n$ dimensions. 
\item[Notebook:] web application for the creation and share of documents that include:\\
\begin{itemize}
\item code
\item figures and animations
\item text, including equations
\end{itemize}
\item[Virtual research environment (\textbf{VRE}):] online infrastructure which aims to help researchers to work collaboratively by managing
\begin{itemize}
\item the software tools, services and technologies, 
\item the access to data resources, public or private,
\item the publication of the results obtained. 
\end{itemize}

\end{description}
\end{block}

\begin{block}{\secnum{2}~Notebooks: what exists today?}
\justifying



\begin{figure}
\parbox{.55\columnwidth}{
\includegraphics[width=.525\columnwidth,frame]{beaker}
}\parbox{.45\columnwidth}{
\caption{Beaker (\url{​http://beakernotebook.com/​})\\
Notebook-style development environment for working interactively with large and complex datasets. 
\begin{itemize}
\item[\Checkmark] Usage of different languages in different cells,\\ 
within the same notebook
\item[\Checkmark] Language manager
\end{itemize}}	
}
\end{figure}

\begin{figure}
\parbox{.55\columnwidth}{
\includegraphics[width=.525\columnwidth,frame]{cocalc2}
}\parbox{.45\columnwidth}{
\caption{"\textit{Collaborative Calculation in the Cloud}" (\url{​https://cocalc.com/​})\\
Web-based cloud computing platform, formerly called formerly called \texttt{SageMathCloud}. 
\begin{itemize}
\item[\Checkmark] Support of many languages
\item[\Checkmark] Users to upload their file on the platform\\
 to be later read or processed
\end{itemize}}	
}
\end{figure}

\begin{figure}
\parbox{.55\columnwidth}{
\includegraphics[width=.525\columnwidth,frame]{Rmarkdown}
}\parbox{.45\columnwidth}{
\caption{R-Markdown (\url{​http://rmarkdown.rstudio.com/​})\\
Dynamic, self-contained documents with embedded chunks of code. 
\begin{itemize}
\item[\Checkmark] Possible to export in journal (\url{​https://github.com/rstudio/rticles​}) or 
presentation formats
\item[\Checkmark] \LaTeX templates to ensure journal standards
\end{itemize}}
}
\end{figure}

\begin{figure}
\parbox{.55\columnwidth}{
\includegraphics[width=.525\columnwidth,frame]{Zeppelin}
}\parbox{.45\columnwidth}{
\caption{Apache Zeppelin (\url{​https://zeppelin.apache.org/​})\\
Web-based notebook for data-driven, interactive and collaborative documents.\\
Intended for \textit{big data} and large scale projects.
\begin{itemize}
\item[\Checkmark] Languages can be mixed in the same notebook
\item[\Checkmark] Users can write their own interpreter (\textit{language backend})
\end{itemize}}
}
\end{figure}


\end{block}

\end{column}

% 2nd column
%-------------------------------------------------------------------------------

\begin{column}{.3\linewidth}
  

\begin{block}{}

\begin{figure}
\parbox{.55\columnwidth}{
\includegraphics[width=.525\columnwidth,frame]{jupyter}
}\parbox{.45\columnwidth}{
\caption{Jupyter (\url{​http://jupyter.org/})\\
Web application for the creation and sharing of notebook-type documents.\\
Evolved from ​IPython​, a command shell for interactive computing (2001). 
\begin{itemize}
\item[\Checkmark] More than 40 language \textit{kernels} available
\item[\Checkmark] Can be used as a multi-user server (\texttt{​jupyterhub​})\\
$\rightarrow$ avoid installation steps on several users' machine
\end{itemize}}
}
\end{figure}

\end{block}

\begin{block}{\secnum{3}~Why do we use Jupyter?}

\begin{enumerate}
\item Open source\ldots\hfill as the others
\item Many programming languages\ldots\hfill as the others
\item Easy installation\ldots\hfill as some of the others
\item A nice solution to deploy on a cloud:\hfill \important{JupyterHub}
\end{enumerate}

\begin{tikzpicture}[remember picture,overlay]
\node [scale=6,text opacity=0.1]
at (17,-4) {Comparison};
\end{tikzpicture}

\begin{table}
\scriptsize
\begin{tabular}{C{.14\columnwidth}L{.155\columnwidth}L{.155\columnwidth}L{.155\columnwidth}L{.155\columnwidth}L{.155\columnwidth}}
\toprule
\textbf{Tool name}		& R-Markdown	& Jupyter	& Beaker	& Cocalc & Zeppelin\\
\midrule
\faGithub \hspace{2cm}\url{https://github.com/...}	& 
\url{.../rstudio/rmarkdown} & 
\url{.../jupyter/notebook} &
\url{.../twosigma/beakerx} &
\url{.../sagemathinc/cocalc} &
\url{.../apache/zeppelin} \\
\hline
\textbf{Languages}	& R, Python, SQL, Bash, Rcpp, Stan, JavaScript &
Julia, Python, R, Scala, Bash, Octave, Rubi, Fortran, PHP, \ldots&
Julia, Python, R, Javascript, C++, Torch, Scala, Bash, Octave, Rubi, Fortran, \ldots &
R​, Python, Octave​, Cython, ​Julia, Java, C/C++, Perl, Ruby &
Scala, Python, SparkSQL, Hive, Markdown \\
\hline
\textbf{Export formats} & 
HTML, PDF, MS Word, Beamer, HTML5 slides, \ldots & 
PDF, LaTeX, HMTL, Markdown, reST & 
Beaker format & 
& JSON \\
\hline
\textbf{Cloud deployment}	& -- & \cellcolor{Yellow}JupyterHub & Beaker Lab {\tiny (discontinued)} & -- & Yes \\
\bottomrule
\end{tabular}
\end{table}

\end{block}


\begin{block}{\secnum{4}~A few more words about  \raisebox{-.5ex}{\includegraphics[height=3ex]{logo_hub}}}
\justifying
\important{Multi-user} Hub which $\left\{\begin{tabular}{l}
spawns\\
manages\\
proxies
\end{tabular}\right\}$ multiple instances of the \important{single-user} Jupyter notebook server (\faGithub~\url{https://github.com/jupyterhub/jupyterhub}).

\begin{figure}
\begin{tikzpicture}
\node[anchor=south west,inner sep=1cm] (image) at (0,0) {\includegraphics[width=0.9\textwidth]{./figures/jupyter_hub_sdc.png}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
\draw[dashed,-stealth,thick, DarkOrange] (.25,.22) to[out=-90, in=90] (0.12,0.15) node[below,text width=6cm,text centered] {Available files and directories};
\draw[dashed,-stealth,thick, DarkOrange] (.15,.75) to[out=90, in=-90] (0.3,0.8) node[above,text width=6cm,text centered] {Running notebooks and terminals};
\draw[dashed,-stealth,thick, DarkOrange] (.9,.65) to[out=90, in=0] (0.7,0.7) node[left,text width=8cm,text centered] {New notebooks with available language kernels};
        
%        \draw[help lines,xstep=.1,ystep=.1] (0,0) grid (1,1);
%        \foreach \x in {0,1,...,9} { \node [anchor=north] at (\x/10,0) {0.\x}; }
       % \foreach \y in {0,1,...,9} { \node [anchor=east] at (0,\y/10) {0.\y}; }
    
\end{scope}
\end{tikzpicture}
\caption{Test instance of jupyterhub deployed for the SeaDataCloud VRE.}
\end{figure}

\important{Spawner:} responsible for the start of the computer environment for the user, either directly on the
server or on a cluster. Several spawners are available, among them DockerSpawner, which enables JupyterHub to spawn single user notebook servers in Docker containers (\faGithub~\url{https://github.com/jupyterhub/dockerspawner}).

\end{block}

\begin{block}{\secnum{5}~How will we use notebooks in the VRE?}
\justifying

\begin{center}\large 3 steps into 1\end{center}

With DIVA \hfill with \important{DIVAnd}
\vspace{.25cm}

$\left\{\begin{tabular}{l}
\important{\faFilePdfO}~Read the doc\\
\important{\faCode}~Compile and run the code \\
\important{\faFilePdfO} Document the execution: parameters, configuration, \ldots
\end{tabular}\right\}$ $\rightarrow$ \hfill \hfill \important{Run the notebook!}

\end{block}


\end{column}


% 3rd column
%-------------------------------------------------------------------------------

\begin{column}{.3\linewidth}


\begin{block}{\secnum{6}~Anatomy of a notebook}

Notebooks contain:
\begin{enumerate}
\item Text cell to document the code
\item Cell codes that can be exectuted piece by piece
\item Results from the code execution
\item Figures or animations
\end{enumerate}

\begin{figure}
\begin{tikzpicture}
\node[anchor=south west,inner sep=1cm] (image) at (0,0) {\includegraphics[width=0.9\textwidth,frame]{./figures/notebook_descr1.png}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
\draw[dashed,-stealth,ultra thick, DarkOrange] (.17,.8) to (0.3,0.8) node[right] {Text cell for documenting (\includegraphics[height=1.5ex]{logo_markdown.png})};
\draw[dashed,-stealth,ultra thick, DarkOrange] (.5,.7) to (0.6,.7) node[right] {Code cell to be executed};
\draw[dashed,-stealth,ultra thick, DarkOrange] (.3,.55) to (0.4,.55) node[right] {Result of the cell execution};
\draw[dashed,-stealth,ultra thick, DarkOrange] (.08,.27) to[out=-90, in=180] (0.6,-.1) node[right] {Order of execution};
\end{scope}
\end{tikzpicture}

\begin{tikzpicture}
\node[anchor=south west,inner sep=1cm] (image) at (0,0) {\includegraphics[width=0.9\textwidth,frame]{./figures/notebook_descr2.png}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
\draw[dashed,-stealth,ultra thick, DarkOrange] (.15,.9) to[out=0,in=180] (0.2,0.87) node[right] {Code cell to generate the plot};
\draw[dashed,-stealth,ultra thick, DarkOrange] (.7,.7) to[out=45, in=-90] (0.7,.95) node[above] {Figure obtained from the cell execution};
% \draw[help lines,xstep=.1,ystep=.1] (0,0) grid (1,1);
% \foreach \x in {0,1,...,9} { \node [anchor=north] at (\x/10,0) {0.\x}; }
% \foreach \y in {0,1,...,9} { \node [anchor=east] at (0,\y/10) {0.\y}; }
\end{scope}
\end{tikzpicture}

\caption{Examples of notebook cells.}
\end{figure}


\end{block}

\begin{block}{\secnum{7}~Next steps}
\begin{itemize}
\item[\Checkmark] Use the files produced by \important{webODV} as an input for \important{DIVAnd} 
\item[\Checkmark] Build a \important{RESTful API} to make easier the integration into the VRE
\item[\Checkmark] Publish the notebooks along with the data and the products
\end{itemize}
\end{block}

\begin{block}{Acknowledgements}
\justifying
The work presented in this poster has been developed in the frame of\\
\important{SeaDataCloud}--\textit{Further developing the pan-European infrastructure for marine and ocean data management}, Project ID~730960 and\\
\important{ODIP~2}--\textit{Extending the Ocean Data Interoperability Platform}, Project ID: 654310.
\vspace{.5cm}

\includegraphics[height=2cm]{logo_odip}~~\includegraphics[height=2cm]{logo_seadatacloud}

\end{block}


\end{column}

\end{columns}

\vfill

\end{frame}

\end{document}





