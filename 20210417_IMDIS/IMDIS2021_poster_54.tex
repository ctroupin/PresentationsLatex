\documentclass[11pt,a4paper,svgnames,final]{beamer}
\usepackage[orientation=portrait,scale=.9]{beamerposter}
\usepackage{graphicx}
\usepackage{url}
\usepackage[skins]{tcolorbox}
\usepackage{fontawesome}
\usepackage{academicons}
\usepackage{tikz}
\usepackage{setspace}
\usepackage{latexsym}
\usepackage{geometry}
\geometry{paperwidth=50cm,paperheight=50cm,margin=.2cm}
\parskip .25cm


\tcbuselibrary{raster}

\setbeamertemplate{navigation symbols}{}

% Define set of colors
\definecolor{textcolor}{HTML}{263541}
\definecolor{highlightext}{HTML}{189E88}
\definecolor{complementary}{HTML}{9E4928}
\definecolor{background}{HTML}{FFFFFF}

\hypersetup{bookmarksopen=true,
bookmarksnumbered=true,  
pdffitwindow=false, 
pdfstartview=FitH,
pdftoolbar=false,
pdfmenubar=false,
pdfwindowui=true,
pdfauthor=Charles Troupin,
pdftitle=IMDIS2021,
pdfsubject=Interpolation tools,
colorlinks=true,%
breaklinks=true,%
linkcolor=highlightext,anchorcolor=highlightext,%
citecolor=textcolor,filecolor=textcolor,%
menucolor=textcolor,%
urlcolor=complementary}

\newcommand{\putlogo}[1]{\includegraphics[height=.6	cm]{#1}}
\newcommand{\putlogoproj}[1]{\includegraphics[height=.7cm]{#1}}


\newcommand*\circled[1]{\tikz[baseline=(char.base)]{
            \node[shape=circle,inner sep=2pt,fill=complementary,draw=highlightext,thick] (char) {#1};}}

\setbeamercolor{projected text}{bg=textcolor}
\newfontfamily\titlefont[Path = /home/ctroupin/.fonts/]{Transformers Movie.ttf}
\newfontfamily\subtitlefont[Path = /home/ctroupin/.fonts/]{NasalizationRg-Regular2.ttf}
\newfontfamily\paperfont[Path = /home/ctroupin/.fonts/,Extension = .ttf,
UprightFont = *Regular,
BoldFont = CalibriBold,
ItalicFont = CalibriItalic,
BoldItalicFont = CalibriBoldItalic]{Calibri}

\DeclareUrlCommand\doi{\textcolor{highlightext}{\aiDoi}\def\UrlLeft##1\UrlRight{\nobreakspace\href{http://dx.doi.org/##1}{##1}}\urlstyle{sf}}
\newcommand{\mycheck}{\textcolor{highlightext}{\faCheckCircle}}


\setbeamerfont{title}{size=\fontsize{40}{45}\selectfont,series=\titlefont}
\setbeamerfont{projected text}{size*={24}{24},series=\subtitlefont}
\setbeamerfont{caption}{size=\fontsize{10}{8},series=\subtitlefont}
%\setmathsfont{series=\subtitlefont}

\setbeamertemplate{caption}[numbered]

\setbeamercolor{caption name}{fg=highlightext}


\newcommand{\montant}{\vphantom{\circled{10}}}

  
\newcommand{\skbox}[1]{\textcolor{complementary}{#1}}


\begin{document}


\begin{tcbitemize}[lower separated=true,raster columns=3,raster rows=3,raster height=0.95\linewidth,valign=center,
raster every box/.style={opacityback=1,size=small,colframe=highlightext,colback=background,coltitle=background,
fonttitle=\large\subtitlefont,fontupper=\fontsize{11}{10}\subtitlefont,fontlower=\fontsize{11}{10}\subtitlefont,collower=textcolor,colupper=textcolor,
segmentation style={dashed, highlightext,line width=1.5pt,opacity=.8},}]

\tcbitem[title=\circled{1}~The situation\montant,sidebyside,righthand width=.022\textwidth,parbox=false,fontupper=\fontsize{11}{12}\subtitlefont]

%Thanks to an always improving observing network, the ocean reveals some of its secrets. 

Spatial interpolation of in situ and remote-sensing observations has been performed for decades in oceanography, with several applications:

\mycheck~simple visualisation\\
\mycheck~initialisation of numerical models\\
\mycheck~quality control of observations

We will present 3 tools designed for the interpolation of oceanographic data.

\begin{figure}
\centering
\includegraphics[width=.6\textwidth]{oxygen_DIVAnd_WOA_month_01_0000.jpg}
\caption{Gridded oxygen concentration from WOA and from EMODnet Chemistry.}

\end{figure}


\tcblower

\LARGE \faArrowCircleRight

\tcbitem[title=\circled{2}~The problems\montant,sidebyside,righthand width=.022\textwidth,colback=white]  

\textcolor{complementary}{Observing the ocean} is complex:\\
variety of processes and spatial scales\\
always changing conditions\\
presence of clouds that prevents satellite observations 

\vspace{.15cm}
\textcolor{complementary}{Oceanographic data interpolation} is complex:\\
\faTimesCircle~~Uneven data coverage\\
\faDatabase~~Large amount of data to process\\
\faGlobe~~Presence of physical boundaries (coastlines, land)\\

\begin{figure}
\centering
\includegraphics[height=4cm]{EMODnet_data_domains_phosph.png}~\includegraphics[height=4cm]{SNPP_VIIRS-20210128T141201-L2-SST.png}
\caption{$\nwarrow$~data locations for the phosphate concentration, taken from EMODnet Chemistry. 
$\nearrow$~sea surface temperature on January 28, 2021, as measured by SNPP/VIIRS.}
\end{figure}


\tcblower

\LARGE \faArrowCircleRight

%------------------------------------------------------------------
\tcbitem[title=\circled{3}~Tool I: DINEOF\montant,colback=white] 

\skbox{Method} Empirical Orthogonal Function (EOF) based approach to fill in missing data from geophysical fields, typically because of the presence of clouds
\vspace{.15cm}

\skbox{Applications} Sea surface temperature (SST), sea surface salinity (SSS), chlorophyll concentration or suspended particulate matter (SPM)
\vspace{.15cm}

\skbox{New developments} Detection and removal of cloud shadows in high-resolution images.\\
Combination of data from sensors with different spatial and temporal resolutions, for instance Sentinel-2, Sentinel-3 and SEVIRI. 


\begin{figure}
\centering
\parbox{.66\textwidth}{
\includegraphics[width=.5\textwidth]{sentinel3_log_0027.png}
}\parbox{.33\textwidth}{
\caption{Reconstruction of Sentinel-3 suspended particular matter on April 25, 2017.}
}
\end{figure}
\vspace{-.5cm}
\begin{figure}
\centering
\parbox{.725\textwidth}{
\includegraphics[width=.33\textwidth]{S2-L2_C2RCC_visible_2017-09-13.png}~\includegraphics[width=.33\textwidth]{S2-L2_C2RCC_visible_2017-09-13_noshadow.png}
}\parbox{.25\textwidth}{
\caption{Sentinel-2 SPM before and after cloud shadow removal}
}
\end{figure}


\tcblower

\begin{center}
\LARGE \faArrowCircleDown
\end{center}
%------------------------------------------------------------------
\tcbitem[title=\circled{7}~About the authors\montant,fontupper=\fontsize{8}{10}\subtitlefont] 

\begin{tabular}{p{.3\textwidth}p{.3\textwidth}p{.3\textwidth}}

Charles Troupin 											& Aida \textbf{Alvera-Azcárate} 				& Alexander Barth \\
\textcolor{highlightext}{Researcher} & \textcolor{highlightext}{Researcher}				& \textcolor{highlightext}{FNRS research associate}   \\
\putlogo{logo_gher.png}~\putlogo{logo_uliege.jpeg}& \putlogo{logo_gher.png}~\putlogo{logo_uliege.jpeg}	& \putlogo{logo_gher.png}~\putlogo{logo_uliege.jpeg}	\\
\includegraphics[width=.3\textwidth]{charles.jpg}	& \includegraphics[width=.3\textwidth]{aida.JPG}	& \includegraphics[width=.3\textwidth]{alex.jpg} \\
& & \\
& & \\
Matjaž Ličer								& Dimitry Van der Zande 					& Jean-Marie Beckers\\
\textcolor{highlightext}{Scientific associate}	& \textcolor{highlightext}{Remote sensing scientist}			& \textcolor{highlightext}{Profesor}			 \\
\putlogo{NIB_logo.jpg}						& \putlogo{rbins.png} & \putlogo{logo_gher.png}~\putlogo{logo_uliege.jpeg}\\
\includegraphics[width=.3\textwidth]{matjaz.jpg} 	& \includegraphics[width=.3\textwidth]{vanderzande3.jpg}	& \includegraphics[width=.3\textwidth]{jmb.JPG} \\

\end{tabular}

\vspace{1cm}
Contact: \faEnvelopeO~~\href{mailto:ctroupin@uliege.be}{ctroupin@uliege.be} \hspace{1cm}\faTwitter~\@CharlesTroupin

%%------------------------------------------------------------------
\tcbitem[fontlower=\fontsize{12}{10}\subtitlefont,valign=center,boxrule=8pt] 

\vspace{2cm}

\centering
{\usebeamerfont{title}{\selectfont Filling data gaps through \textcolor{highlightext}{interpolation}: innovative analysis tools for \textcolor{highlightext}{oceanography}}}

\tcblower

\vspace{2cm}

\begin{eqnarray*}
\textsf{DIVAnd} &=& \textsf{Data-interpolating variational analysis in n dimensions}\\
\textsf{DINEOF} &=& \textsf{Data-interpolating Empirical Orthogonal functions}\\
\textsf{DINCAE} &=& \textsf{Data-Interpolating Convolutional Auto-Encoder}
\end{eqnarray*}



%------------------------------------------------------------------
\tcbitem[title=\circled{4}~Tool II: DIVAnd\montant] 

\skbox{Objective} Generation of a gridded field using in situ measurements, in an arbitrary high dimensional space
\vspace{.15cm}

\skbox{Method} Minimisation of a cost function that takes into account the proximity to the observations, the smoothness of the interpolated field and the presence of coastlines
\vspace{.15cm}

\skbox{Applications} 

\putlogoproj{logo_emodnet.png}~Chemistry interpolated maps

\putlogoproj{logo_seadatacloud.jpeg}~Climatologies

\begin{figure}
\centering
\includegraphics[width=.65\textwidth]{BlackSea_salinity.png}
\caption{Winter salinity at 10~m depth in the Black Sea.}
\end{figure}

\tcblower

\begin{center}
\LARGE \faArrowCircleDown
\end{center}
%------------------------------------------------------------------
\tcbitem[title=\circled{6}~Code and publications\montant]

All the software tools are open source and the codes available on GitHub \faGithub\\
\url{https://github.com/gher-ulg/DIVAnd.jl} \hfill (Julia)\\
\url{https://github.com/aida-alvera/DINEOF} \hfill (Fortran)\\
\url{https://github.com/gher-ulg/DINCAE} \hfill (Python, Julia)\\

\skbox{Publications}

\vspace{.25cm}

{
\paperfont

Alvera-Azcárate, A.; Van der Zande, D.; Barth, A.; Cardoso dos Santos, J. F.; Troupin, C. \& Beckers, J.-M. Detection of shadows in high spatial resolution ocean satellite data using DINEOF. \textit{Remote Sensing of Environment}, 2021, \textbf{253:} 112229. \doi{10.1016/j.rse.2020.112229}
\vspace{.15cm}

Barth, A.; Troupin, C.; Reyes, E.; Alvera-Azcárate, A.; Beckers, J.-M. \& Tintoré, J. Variational interpolation of high-frequency radar surface currents using DIVAnd. \textit{Ocean Dynamics}, 2021, \textbf{71:}, 293--308. \doi{10.1007/s10236-020-01432-x}
\vspace{.15cm}

Barth, A.; Alvera-Azcárate, A.; Licer, M. \& Beckers, J.-M. DINCAE 1.0: a convolutional neural network with error estimates to reconstruct sea surface temperature satellite observations. \textit{Geoscientific Model Development}, 2020, \textbf{13:} 1609--1622. \doi{10.5194/gmd-13-1609-2020}
\vspace{.15cm}

Barth, A.; Beckers, J.-M.; Troupin, C.; Alvera-Azcárate, A. \& Vandenbulcke, L. divand-1.0: n-dimensional variational data analysis for ocean observations. \textit{Geoscientific Model Development}, 2014, \textbf{7:} 225--241. \doi{10.5194/gmd-7-225-2014}

}
%------------------------------------------------------------------
\tcbitem[title=\circled{5}~Tool III: DINCAE, sidebyside,lefthand width=.022\textwidth]

{\LARGE \faArrowCircleLeft}

\tcblower

\vspace{.5cm}

\skbox{Objective} Reconstruction of the missing data based on the available cloud-free pixels in satellite images. 
\vspace{.15cm}

\skbox{Method} Neural network with the structure of a convolutional auto-encoder 
\vspace{.15cm}

Requires a method to handle missing data (or data with variable accuracy) in the training phase. 

%DINCAE introduces a consistent approach which uses the satellite data and its expected error variance as input and provides the reconstructed field along with its expected error variance as output. 

The neural network is trained by maximizing the likelihood of the observed value. 
\vspace{.15cm}

\skbox{Application} reconstruction of 25-year time-series of Advanced Very High Resolution Radiometer (AVHRR) SST data  
\vspace{.15cm}

\skbox{Results} The reconstruction error of both approaches is computed using cross-validation and in situ observations from the World Ocean Database. DINCAE results have lower error, while showing higher variability than the DINEOF reconstruction.

\begin{figure}
\centering
\parbox{.66\textwidth}{
\includegraphics[height=5cm]{gmd-13-1609-2020-f07-web.png}
}\parbox{.33\textwidth}{
\caption{Original SST and reconstructions with DINCAE and DINEOF.} 
}
\end{figure}


%------------------------------------------------------------------
\tcbitem[title=DIVAnd,sidebyside,lefthand width=.022\textwidth] %\includegraphics[width=.99\textwidth]{Recollets2.JPG}

{\LARGE \faArrowCircleLeft}

\tcblower

\skbox{New developments} 
\vspace{.15cm}


\putlogoproj{logo_emodnet.png}~Biology: interpolation of presence/absence data with DIVAnd coupled with a neural network

\begin{figure}
\centering
\parbox{.66\textwidth}{
\includegraphics[height=3.75cm]{Ampelisca_brevicornis_density_masked.jpg}~\includegraphics[height=3.75cm]{Ampelisca_brevicornis_error.jpg}
}\parbox{.33\textwidth}{
\caption{$\leftarrow$~Map of probability of presence and $\rightarrow$ error field for "Ampelisca brevicornis".} 
}
\end{figure}


\putlogoproj{logo_emodnet.png}~Physics: interpolation of sea surface velocity as measured by high-frequency radar
\begin{figure}
\centering
\parbox{.66\textwidth}{
\includegraphics[width=4cm]{radials_grid_legend.png}~\includegraphics[height=4cm]{analysis_V1_constr.jpg}
}\parbox{.33\textwidth}{
\caption{Radial velocities and interpolated field obtained combining 4 sites on March~5, 2015}
}
\end{figure}

\putlogoproj{logo_bluecloud.png}: interpolation of abundance data for different species


\end{tcbitemize}

\end{document}