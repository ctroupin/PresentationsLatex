\documentclass[final]{beamer}
\mode<presentation>
{
  \usetheme{GHERposterDark}
}

\hypersetup{bookmarksopen=true,
bookmarksnumbered=true,  
pdffitwindow=false, 
pdfstartview=FitH,
pdftoolbar=true,
pdfmenubar=true,
pdfwindowui=true,
linkcolor=bluegher,
pdfsubject={},
pdfkeywords={},
bookmarksopenlevel=1,
colorlinks=true,urlcolor=bluegher,
citecolor=blue}

\graphicspath{
{./},
{../../figures4presentations/logo/},
{./logos/},
}

\newcommand{\thecheck}{\textcolor{mygreen}{\checkmark}}


% Uncomment next line to display a grid to help align images
%\beamertemplategridbackground[1cm]

\title[Eutrophication datasets]{Spatial and temporal patterns of eutrophication:\\ a new data product for the European Seas}
\author{Charles~Troupin, Alexander~Barth, Karin~Wesslander, Lotta~Fyrberg, Julie~Gatti, Quitterie~Cazenave, Gwenaël~Caer, Jonas~Koefoed R{\o}mer,
Martin M{\o}rk~Larsen, Luminita~Buga, George~Sarbu,\\ Sissy~Iona, Marilena~Tsompanou, Ann Kristin~{\O}strem, Maria Eugenia~Molina~Jack, Reiner Schlitzer, Sebastian~Mieruch, Dick~Schaap, Marina~Lipizer \& Alessandra~Giorgetti}
\institute{GeoHydrodynamics and Environment Research, University of Liège}
\date{\today}

\begin{document}
\begin{frame}{}



\begin{center}

\includegraphics[height=2cm]{emodnet}\hspace{3cm}
\includegraphics[height=2cm]{uliege}\hspace{2cm}\includegraphics[height=2.cm]{smhi}\hspace{2cm}\includegraphics[height=2.cm]{ifremer}\hspace{2cm}\includegraphics[height=2cm]{aarhus}\hspace{2cm}\includegraphics[height=2cm]{incdm}\hspace{2cm}\includegraphics[height=2cm]{hcmr}\hspace{2cm}\includegraphics[height=2cm]{imr}\hspace{2cm}\includegraphics[height=2cm]{ogs}\hspace{2cm}\includegraphics[height=2cm]{awi}\hspace{2cm}\includegraphics[height=2cm]{maris}

\vspace*{1.5cm}

\begin{minipage}{0.95\textwidth}
\textit{\Large The state and the evolution of eutrophication within the European Seas are of paramount importance for many stakeholders, in particular in the frame of the implementation of European Union (EU) marine policies such as the Marine Strategy Framework Directive (MSFD), Water Framework Directive (WFD), and Maritime Spatial Planning Directive (MSP). The EMODnet Chemistry eutrophication datasets and products are presented in this poster.}
\end{minipage}
\end{center}

\vspace{1cm}

\begin{columns}[T]
  
% 1st column %-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\begin{column}{.25\linewidth}
    
    
\begin{block}{The datasets}
\justifying
The \textit{Eutrophication and Acidity} aggregated datasets of EMODnet Chemistry consist of data collections gathering in particular measurements of oxygen (Figure~\ref{fig:oxy}), inorganic and organic nitrogen and phosphorus,silicate and chlorophyll-a concentration. 

They are created by aggregating in situ observations over 6 different regions and applying rigorous quality control, leading to freely available, high-quality data collections, made available though the EMODnet Central Portal catalogue.



\end{block}

\vspace*{1cm} 

\begin{block}{The products}
\justifying
Gridded products have been constructed with the DIVAnd software tool, for different areas and at different spatio-temporal resolutions. 

\begin{figure}
\centering
\parbox{.65\textwidth}{
\includegraphics[width=.6\textwidth]{all_domains.jpg}
}\parbox{.35\textwidth}{
\caption{Global, regional and coastal domains.\label{fig:domains}}
}
\end{figure}

\begin{description}[style=unboxed,leftmargin=0cm]
\item[All European Seas:] they cover the largest domain and consist of monthly fields for each variable, calculated on a 0.25° $\times$ 0.25° grid, for the 1960-2020 time period.
\item[Sea Regions:] they focus on 6 European Sea basins, are calculated for 6-year periods for each season, and have a spatial resolution depending on the area: North Sea, Black Sea, Baltic Sea, Mediterranean Sea, Arctic Ocean and Northeast Atlantic Ocean. 
\item[Coastal areas:] they provide high-resolution, seasonal, gridded fields near major river mouths: Gulf of Riga, Danube Delta, Po River and Loire River (shown in red in Figure~\ref{fig:domains}).
\end{description}

\end{block}

\vspace*{2cm} 
 
\begin{block}{The tool}
\justifying

DIVAnd stands for \textit{Data-Interpolating Variational Analysis in n dimensions}. It is a software tool designed to interpolate in situ measurements onto a regular grid. 
It is written in Julia (\url{https://julialang.org/}), a fast and modern programming language.\\
\doi{10.5281/zenodo.1303229}

\begin{figure}
\centering
\includegraphics[height=4cm]{julia}~~~\includegraphics[height=4cm]{divand}~~~\includegraphics[height=4cm]{jupyter}
\end{figure}

A set of Jupyter notebooks is provided to the regional leaders, which they adapt to their specific region: 
\begin{itemize}
\item[\thecheck] domain of interest, 
\item[\thecheck] analysis parameters, 
\item[\thecheck] reference fields and
\item[\thecheck] data transformation (logarithm, \ldots)
\end{itemize}
\doi{10.5281/zenodo.1248029}

\vspace{.25cm}

The GEBCO bathymetry is used to create land-sea mask, which delimits the region where the analysis is performed.

\begin{figure}
\centering
\parbox{.65\textwidth}{
\includegraphics[width=.6\textwidth]{GEBCO}
}\parbox{.35\textwidth}{
\caption{GEBCO bathymetry.}
}
\end{figure}

\end{block}

\end{column}

% 2nd column
%-------------------------------------------------------------------------------

\begin{column}{.45\linewidth}
   
\begin{block}{}
\justifying
\begin{figure}
\centering
\includegraphics[width=\textwidth]{oxygen_6_0.jpg}
\caption{Measurements of oxygen concentration (all months and years) near the surface.\label{fig:oxy}}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=.9\textwidth]{oxygen_DIVAnd_field_month_03_0050.jpg}
\caption{Interpolated oxygen concentration field at 50~m depth in March (climatology).\label{fig:oxyfield}}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=.9\textwidth]{silicate_residuals}
\caption{Residuals for the water body silicate concentration.\label{fig:residuals}}
\end{figure}

\end{block}

\end{column}


% 3rd column
%-------------------------------------------------------------------------------

\begin{column}{.25\linewidth}

\begin{block}{Results}
\justifying
The netCDF files contain:
\begin{description}[style=unboxed,leftmargin=0cm]
\item[the full interpolated field:] the un-masked field covering the region of interest (example in Figure~\ref{fig:oxyfield}).
\item[the error field:] it measures the degree of confidence that one can have in the analysis. It depends mainly on the data distribution.
\item[the masked fields:] using the error field, one can \textit{mask} the analysis field in region where the error is larger than a selected threshold, typically 30\% or 50\% or relative error.
\item[the deepest value fields:] they contain the interpolated values at the deepest level. Those fields are also provided in their \textit{masked} version.
\end{description}

\begin{figure}
\centering
\includegraphics[width=.95\textwidth]{CPviewer_phosphate}
\caption{Sea surface phosphate concentration displayed by the Central Portal viewer: \url{https://emodnet.ec.europa.eu/geoviewer/}.\\ The results are available in the Central Portal catalogue: \url{https://emodnet.ec.europa.eu/geonetwork/emodnet/eng/catalog.search}.}
\end{figure}

A secondary set of results consist of a list of residual values (Figure~\ref{fig:residuals}). A residual is defined as the difference between a given observation and the analysed field at the location and time of the observation. 

In practice, the lists of residuals are sent to the corresponding regional leaders, who then assess the data quality by confirming or not that the observations are considered as \textit{bad data} and need to be discarded.

\end{block}

\vspace{.5cm}

\begin{block}{Metadata}
\justifying
The metadata are prepared at the same time as the gridded fields, in the forms of XML files. Those files contain all the relevant pieces of informations related to the product:

\begin{itemize}
\item[\thecheck] region and period of interest,
\item[\thecheck] a description of the product,
\item[\thecheck] the list of all the data providers, 
\item[\thecheck] the links for download and to visualisation.
\end{itemize}

The metadata are also used to describe the products in catalogues, such as Sextant or the EMODnet Central Portal (Figure~\ref{fig:cpcatalog}).

\begin{figure}
\centering
\includegraphics[width=\textwidth]{centralportal_catalog.png}
\caption{Example of available "dissolved inorganic nitrogen" products in the Central Portal catalogue.\label{fig:cpcatalog}}
\end{figure}

\end{block}

\vspace{.5cm}

\begin{block}{Conclusions}
\justifying

DIVAnd maps are build upon quality-controlled datasets prepared by the regional leaders of 6~different regional seas.\\
The interpolation method allows users to generate gridded field using up to several millions of observations.\\

The tool also provides XML files containing all the relevant fields of metadata.

The data collections and products stem from the combined knowledge of a wide range of scientists and experts, making them unique and essential in the context of Climate Change.
\end{block}

\end{column}


\end{columns}


\end{frame}

\end{document}





