\input{EGU2022.tex}
\usepackage[export]{adjustbox}
\defbeamertemplate{description item}{align left}{\insertdescriptionitem\hfill}

\title[Interpolation of high-frequency\\ radar velocities]{Interpolation of high-frequency radar velocities}
\author{Charles~ Troupin, Alexander Barth,\\ Marco Alba and Antonio Novellino}
\subtitle{EGU 2022 General Assembly\\Oceanography at coastal scales.\\Modelling, coupling, observations and applications}
\institute{GHER~-~ULiège}
\date{23--27~May, 2022}
\titlegraphic{\includegraphics[height=\logoheight]{logo_uliege.jpeg}~\includegraphics[height=\logoheight]{logo_gher}~\includegraphics[height=\logoheight]{logo_ett}
}

\definecolor{highlightext}{HTML}{8E8E8E}

\urlstyle{rm}

\begin{document}

{
%\usebackgroundtemplate{
%{\includegraphics[width=\paperwidth]{backgroundarrows5.png}}
%}

\begin{frame}[plain]

\maketitle

\end{frame}
}


\begin{frame}
\frametitle{What do we do?}

Generate gridded velocity fields\\
using radial velocities from high-frequency radar systems
 
\begin{figure}[htpb]
\centering
\includegraphics<1>[height=6cm]{radials_grid_legend}
\includegraphics<2>[height=6cm]{radials_Trieste_title.png}
\end{figure}

\end{frame}

\begin{frame}
\frametitle{Why?}

\begin{itemize}
\item Gaps present in original fields
\item Several applications requiring filled fields
\item Many methods available\ldots
\end{itemize}


\end{frame}

\begin{frame}
\frametitle{How do we do that?}

\onslide*<1>{
DIVAnd  = Data-Interpolating Variational Analysis in n dimensions

\vspace{1cm}

\includegraphics[width=\textwidth]{divand_github.png}

}
\onslide*<2>{
Minimisation of a cost function taking into account
\begin{itemize}
\item Observations
\item Regularity of the field
\end{itemize}
}

\onslide*<3>{
Additional constraints, specific to HF radar:
\begin{description}
\footnotesize
\item[]
\item[divergence:] weak divergence of the flow 
\item[boundary conditions:] normal velocity component $\approx 0$
\item[Coriolis force:] velocity at a given time is similar to the velocity before/after
\end{description}

\vspace{1cm}

\includegraphics[width=\textwidth]{divand_hfradar.png}
}

\end{frame}

\begin{frame}
\frametitle{Results}

\onslide*<1>{
Fields generated in 4 regions:
\begin{enumerate}
\item Gulf of Manfredonia (Adriatic coast of Italy), 
\item Gulf of Trieste (north of the Adriatic Sea), 
\item Gulf of Naples (south-western coast of Italy) and 
\item Northeast Gran Canaria island (Atlantic Ocean). 
\end{enumerate}
}

\onslide*<2>{
\begin{description}
\item[Format:] netCDF
\item[Metadata:]  Climate and Forecast (CF) conventions \\
+ recommendation of the EuroGOOS HFR Task Team

\item[Resolution:]

\begin{tabular}{lll}
Region		& Spatial resolution	& Temporal resolution\\
Naples 		& 0.01° $\times$ 0.01°	& 1 hour				\\
Canary 		& 0.01° $\times$ 0.01°	& 1 hour				\\
Trieste 	& 0.01° $\times$ 0.01°	& 30 minutes			\\
Manfredonia	& 0.02° $\times$ 0.02°	& 1 hour				\\
\end{tabular}
\item[Distribution:] through EMODnet Physics 
\end{description}
}

\end{frame}

\begin{frame}
\frametitle{Results}
\begin{figure}[htpb]
\centering
\includegraphics<1>[height=.5\textwidth]{field_Manfredonia_20140811T190000.png}
\includegraphics<2>[height=.5\textwidth]{field_gulftrieste_20160307T043000.png}
\includegraphics<3>[height=.5\textwidth]{field_Gulf-Naples_20120409T000000.png}
\includegraphics<4>[height=.5\textwidth]{field_canary_20211224T030000.png}
\end{figure}
\end{frame}




\begin{frame}[fragile]
\frametitle{Validation}

\onslide*<1>{
Difficult to find \textit{in situ} measurements $\rightarrow$ external datasets
\begin{itemize}
\footnotesize
\item Chlorophyll-concentration (CMEMS product no.\\
\texttt{OCEANCOLOUR\_MED\_CHL\_L3\_NRT\_OBSERVATIONS\_009\_04} and\\
\texttt{OCEANCOLOUR\_MED\_CHL\_L3\_REP\_OBSERVATIONS\_009\_073})\\
for the Gulf of Manfredonia.
\item Velocity from model (CMEMS product no.~\\
\texttt{IBI\_ANALYSISFORECAST\_PHY\_005\_001})\\ 
for Gran Canaria.
\end{itemize}
}

\onslide*<2>{

HF radar vs. chlorophyll concentration

\vspace{.5cm}

\begin{figure}
\centering
\includegraphics[width=.49\textwidth]{field_Manfredonia_20140812T120000.png}~\includegraphics[width=.49\textwidth]{chloro_20148012.jpg}
\end{figure}
}


\onslide*<3>{

HF radar vs. CMEMS IBI model

\vspace{.5cm}

\begin{figure}
\centering
\includegraphics[width=.49\textwidth]{field_canary_20211224T220000.png}~\includegraphics[width=.49\textwidth]{cmems_canary_20211224T223000.png}\\
\end{figure}
}

\onslide*<4>{
HF radar vs. CMEMS IBI model

\vspace{.5cm}

\begin{figure}
\centering
\includegraphics[width=.49\textwidth]{field_canary_20211228T030000.png}~\includegraphics[width=.49\textwidth]{cmems_canary_20211228T033000.png}\\
\end{figure}
}

\end{frame}



\begin{frame}[c]
\frametitle{Credits}
\scriptsize
\begin{tabular}{rl}
\textbf{Cast} & 										\\
Data access			& Marco Alba, Antonio Novellino	\\
Code developer 	& Alexander Barth 						\\
Application design	& Charles Troupin					\\
Validation and parameter optimisation & Basile Caterina, Quentin Renouvel and Hugo Romanelli 				\\
					&									\\
			   \textbf{Code} & 							\\
DIVAnd.jl			& \textcolor{greygher}{\faGithubSquare}~\url{https://github.com/gher-ulg/Divand.jl}\\
DIVAnd\_HFRadar.jl	& \textcolor{greygher}{\faGithubSquare}~\url{https://github.com/gher-ulg/DIVAnd_HFRadar.jl}\\
EMODnet Physics HF Radar & \textcolor{greygher}{\faGitlab}~\url{https://gitlab.uliege.be/gher/diva-emodnetphysics}\\
						& \\
\textbf{Publications} 	& \\
Barth et al. (2021), Ocean Dynamics	& \textcolor{greygher}{\aiDoi}~{10.1007/s10236-020-01432-x}\\
Report									& \textcolor{greygher}{\aiDoi}~{10.5281/zenodo.5811989}\\
						& \\
				\textbf{Funding} &					\\
Projects				& EMODnet Physics, SeaDataCloud\\
Computing (CECI HPC)	& F.R.S.-FNRS \& the Walloon Region
				
			
\end{tabular}

\end{frame}


\end{document}


