\documentclass[draft,svgnames]{beamer}
\usepackage[orientation=landscape,scale=.85]{beamerposter}
\usepackage{tcolorbox}
\usepackage{blindtext}
\usepackage{natbib}
\usepackage{array}
\usepackage{subcaption}


\mode<presentation>
{
  \usetheme{GHERposter}
}

\hypersetup{bookmarksopen=true,
bookmarksnumbered=true,  
pdffitwindow=false, 
pdfstartview=FitH,
pdftoolbar=true,
pdfmenubar=true,
pdfwindowui=true,
pdfsubject={EGU2022},
pdfkeywords={DIVAnd, HF radar, Spatial interpolation, EMODnet Physics},
bookmarksopenlevel=1,
colorlinks=true,urlcolor=bluegher,
citecolor=bluegher}

\DeclareGraphicsExtensions{.pdf,.jpg,.JPG,.png,.PNG}
\graphicspath{
{/home/ctroupin/Presentations/figures4presentations/logo/},
{/home/ctroupin/Projects/EMODnet/Diva-EMODnetPhysics/figures/},
{/home/ctroupin/Projects/EMODnet/Diva-EMODnetPhysics/figures/radar/},
{/home/ctroupin/Projects/EMODnet/Diva-EMODnetPhysics/figures/sensitivity/},
{/home/ctroupin/Projects/EMODnet/Diva-EMODnetPhysics/figures/chloro/},
{/home/ctroupin/Projects/EMODnet/Diva-EMODnetPhysics/figures/chloro/1km/},
{/home/ctroupin/Projects/EMODnet/Diva-EMODnetPhysics/figures/bathymetry/},
{/home/ctroupin/Projects/EMODnet/Diva-EMODnetPhysics/figures/radar/CanaryIslands/},
{/home/ctroupin/Projects/EMODnet/Diva-EMODnetPhysics/figures/radar/Manfredonia/},
{/home/ctroupin/Projects/EMODnet/Diva-EMODnetPhysics/figures/radar/Gulf-Naples/},
{/home/ctroupin/Projects/EMODnet/Diva-EMODnetPhysics/figures/radar/Gulf-Trieste/},
}


\usetikzlibrary{arrows,shapes,backgrounds,spy,calc}
\tikzstyle{every picture}+=[remember picture]
\tikzstyle{na} = [baseline=-.5ex]

\tikzset{
    vertex/.style = {
        circle,
        fill = black,
        outer sep = 0pt,
        inner sep = 1pt,
    }
}

\title[Interpolation of high-frequency radar velocities]{Interpolation of high-frequency radar velocities}
\author{Charles Troupin, Alexander Barth, Marco Alba and Antonio Novellino}
\institute{GeoHydrodynamics and Environment Research, University of Liège}
\date{3-8 April, 2022}


\begin{document}
\begin{frame}[fragile]{} 
\centering
%\vspace{-1cm}
{\tcbset
{colframe=bluegher,nobeforeafter,colupper=white,colback=bluegher,arc=1mm,boxsep=3pt,height=1.5cm,center title}
\raisebox{8pt}{{\LARGE \textcolor{white}{\faTags}}}~~\tcbox{Oceanography}~\tcbox{High-frequency radar}~\tcbox{EMODnet Physics}~\tcbox{Interpolation}\hspace{1cm}\raisebox{10pt}{{\LARGE \textcolor{white}{\faWrench}}}~~\tcbox{Julia}~\tcbox{DIVAnd}~\tcbox{Matplotlib}
}

\rule{\columnwidth}{.2cm}
  
\begin{columns}[T]
  
% 1st column %-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\begin{column}{.31\linewidth}


\begin{block}{Data: radial velocities}
\justifying

\parbox{.5\textwidth}{
The radial velocities are used as the input for the spatial interpolation tools. They are measured by different antennas, depending on the region of interest.
}\parbox{.5\textwidth}{
\begin{figure}[htpb]
\centering
\includegraphics[height=7cm]{Naple_data.png}~\includegraphics[height=7cm]{radials_Trieste_title.png}
\caption{Examples of radials in the Gulf of Naple and the Gulf of Trieste.\label{fig:radial-naples-trieste}}
\end{figure}
}

\end{block}

\begin{block}{Tool: DIVAnd tool}

\parbox{.6\textwidth}{

DIVA \citep[Data-Interpolating Variational Analysis,][]{TROUPIN2012bOM} is a software tool designed to create two-dimensional gridded field from sparsely distributed measurements. It takes into account constraints such as the closeness to the observation or the regularity of the field. 

DIVAnd \citep{BARTH2014} performs the same kind of analysis, but is generalized to an arbitrary number of dimensions (generally time, depth, longitude and latitude). 

DIVAnd was further adapted to perform analysis on radial velocities acquired by high-frequency radar systems \citep{BARTH2021}. Additional constraints are added:
\begin{description}
\item[boundary conditions:] it ensures that the velocity component perpendicular to a physical obstacle, the coast in this case, is close to zero;
\item[divergence:] the divergence of the flow is also expected to be weak;
\item[influence of the Coriolis force:] it ensure that the velocity at given time is similar to the velocity at a previous and next time instance.
\end{description}
}\parbox{.4\textwidth}{
\begin{figure}[htpb]
\centering
\includegraphics[width=.35\textwidth]{bathy_hillshade2_3.png}\\
\includegraphics[width=.35\textwidth]{bathy_hillshade2_4.png}
\caption{Example of high-resolution bathymetries extracted from the EMODnet Bathymetry portal.\label{fig:bathy_hillshade}}
\end{figure}
}

\parbox{.6\textwidth}{The analysis is only performed on grid points located in the sea, as specified by a land-sea mask.
It consists of a matrix filled with 1's (ocean/sea grid points) and 0's (for land grid points). It is directly obtained from a regional bathymetry.
}\parbox{.4\textwidth}{
\begin{figure}[htpb]
\centering
\includegraphics[width=.35\textwidth]{mask_EMODnet_coast2.png}
\caption{Land-sea mask constructed from the bathymetry. The interpolation is only performed on the "sea" points, and is also used for the boundary condition constraint.\label{fig:mask_EMODnet_coast2}}
\end{figure}
}

\end{block}



\end{column}

% 2nd column
%-------------------------------------------------------------------------------

\begin{column}{.31\linewidth}

\begin{block}{Results}

The gridded fields were generated for the following regions:
\begin{enumerate}
\item Gulf of Manfredonia (Adriatic coast of Italy), 
\item the Gulf of Trieste (north of the Adriatic Sea), 
\item the Gulf of Naples (south-western coast of Italy) and 
\item the Gran Canaria island (Atlantic Ocean). 
\end{enumerate}

\begin{figure}[htpb]
\centering
\includegraphics[height=.37\textwidth]{field_Manfredonia_20140811T190000.png}~\includegraphics[height=.37\textwidth]{field_gulftrieste_20160307T043000.png}\\
\includegraphics[height=.37\textwidth]{field_Gulf-Naples_20120409T000000.png}~\includegraphics[height=.37\textwidth]{field_canary_20211224T030000.png}
\caption{Examples of gridded fields in the 4 regions of interest.\label{fig:}}
\end{figure}

\end{block}

\begin{block}{Validation}

The validation cannot always be made using in situ measurements, that is why we relied on other external datasets:
\begin{itemize}
\item Chlorophyll-concentration (CMEMS product no.~ ) for the Gulf of Manfredonia.
\item Velocity measurements (CMEMS product no.~ ) for Gran Canaria.
\end{itemize}


\begin{figure}
\centering
\begin{subfigure}{0.495\textwidth}
\includegraphics[width=\textwidth]{chloro_20148012.jpg}
\caption{Chlorophyll concentration on August 12, 2014.\label{fig:chloro_validA}}
\end{subfigure}
\begin{subfigure}{0.495\textwidth}
\includegraphics[width=\textwidth]{field_Manfredonia_20140812T120000.png}
\caption{Gridded surface currents on August 12, 2014.\label{fig:chloro_validB}}
\end{subfigure}
\caption{Validation of surface current by comparison with chlorophyll concentration from satellite.\label{fig:chloro_valid}}
\end{figure}
\end{block}

\end{column}


% 3rd column
%-------------------------------------------------------------------------------

\begin{column}{.31\linewidth}

\begin{figure}
\centering
\includegraphics[width=.49\textwidth]{field_canary_20211224T220000.png}~\includegraphics[width=.49\textwidth]{cmems_canary_20211224T223000.png}\\
\includegraphics[width=.49\textwidth]{field_canary_20211228T030000.png}~\includegraphics[width=.49\textwidth]{cmems_canary_20211228T033000.png}\\
\caption{Comparison of the reconstructed velocity fields with DIVAnd (left) and the velocity from the IBI model (right).\label{fig:canarycompare}}
\end{figure}


\begin{block}{Code and data availability}

\begin{tabular}{rcl}
Julia language (v1.7.0)		&& \faLink~\url{https://julialang.org/}\\
DIVAnd (v2.7.6)				&& \faGithub~\url{https://github.com/gher-ulg/DIVAnd.jl}\\
DIVAnd HFRadar (v0.1.0)		&& \faGithub~\url{https://github.com/gher-ulg/DIVAnd_HFRadar.jl}\\
DIVA EMODnet Physics		&& \faGitlab~\url{https://gitlab.uliege.be/gher/diva-emodnetphysics}
\end{tabular}


\end{block}

\begin{block}{References and useful links}


\bibliographystyle{copernicus.bst}
\bibliography{hfradar}

\end{block}


\end{column}

\end{columns}

\vfill

\end{frame}

\end{document}





