\input{CPR_style.tex}
\newcommand{\listcheck}{\textcolor{DarkGreen}{\checkmark}}
\newcommand{\listcross}{\textcolor{red}{$\times$}}

\title{CPR\textendash DINCAE}
\subtitle{Progress report}
\author[C.~Troupin]{Charles Troupin}
%\institute{GHER, ULiège}
\date{Last updated: \today\ at \currenttime}
%\titlegraphic{\includegraphics[height=\logoheight]{logo_uliege_square}\includegraphics[height=\logoheight]{logo_gher}}
  
\urlstyle{rm}
\begin{document}
{
%\usebackgroundtemplate{\tikz\node[opacity=0.5] {\hspace{-1cm}\includegraphics[width=1.2\paperwidth]{Athens_2155}}; }
\begin{frame}
\maketitle
\end{frame}
}
%-----------------------------------------------------------------------------------------------



%------------------------------------------------------------------------------
\begin{frame}
\frametitle{}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{calanus_finmarchicus_20231031T135302.jpg}
\end{figure}

\end{frame}

%------------------------------------------------------------------------------
\begin{frame}
\frametitle{How did we get there?}

\begin{itemize}
\item[\listcheck] Gridding with sea surface temperature and bathymetry as \textcolor{darkbluesnd}{environmental} variables
\item[\listcheck] Adding a \textcolor{darkbluesnd}{Laplacian} term in the cost function for the \textit{regularisation} of the gridded field
\item[\listcheck] Applying a \textcolor{darkbluesnd}{$\log_{10}(x+1)$} transformation to the original observations
\item[\listcheck] Removing extreme values (above the 95$^{\mathrm{th}}$ percentile)
\item[\listcheck] Increasing the domain size to avoid boundary issues
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Smoothing effect of the Laplacian term}

{\footnotesize Laplacian penalty coefficients: 10$^{-6}$, 10$^{-5}$, 10$^{-4}$, 10$^{-3}$} 
\vspace*{-.5cm}
\begin{figure}
\centering
\includegraphics[width=.425\textwidth]{calanus_finmarchicus_laplacian_penalty_1.jpg}~\includegraphics[width=.425\textwidth]{calanus_finmarchicus_laplacian_penalty_2.jpg}\\
\includegraphics[width=.425\textwidth]{calanus_finmarchicus_laplacian_penalty_3.jpg}~\includegraphics[width=.425\textwidth]{calanus_finmarchicus_laplacian_penalty_4.jpg}
\end{figure}
\end{frame}

%------------------------------------------------------------------------------
\begin{frame}
\frametitle{Validation of the results}


\begin{description}[leftmargin=-2cm]
\item[\textit{Global} validation:] comparing the mean gridded field (combining all time periods) to the observations.
\item[Cross validation:] discarding observations and checking the gridded field at the locations of the discarded observations.
\end{description}

\end{frame}

%------------------------------------------------------------------------------
\begin{frame}
\frametitle{Cross validation}

\parbox{.55\textwidth}{
\begin{enumerate}
\item Count the number of observations per 2°$\times$2° grid cell and time period
\item Discard 10\% of the cells containing at least one observation
\item Perform the DINCAE gridding with the training dataset
\item Compare the results at the locations where the observations were discarded 
\end{enumerate}
}\parbox{.45\textwidth}{
\includegraphics<1-3>[width=.425\textwidth]{domain_grid_2deg.png}
\includegraphics<4>[width=.425\textwidth]{comparison_2000-02-15.jpg}
}

\end{frame}

%------------------------------------------------------------------------------
\begin{frame}
\frametitle{Hyperparameter setting}

\onslide*<1>{
Different methods:
\begin{description}
\item[Grid search:] a list of possible values (ex. 5 values) is decided for each parameter ($\approx$ 15 parameters), and each combination is tested\\
Number of runs = $\mathcal{O}(5^{15}) = 30\,\,\mathrm{billion}$
\item[Random Search:] create random combinations of parameters
\item[Manual setting:] parameter values set \textit{by hand} 
\end{description}
}

\onslide*<2>{
How did we do it?
\begin{enumerate}
\item Fix the values of some parameters:\\ \textit{skip connections}, \textit{gradient clipping}, \textit{batch size}, \ldots
\item Create random combinations of \textit{learning rate}, \textit{number of epochs}, \textit{Laplacian penalty}\\
(setting an acceptable range)
\item Run DINCAE
\item Compare gridded field with the validation dataset
\end{enumerate}
}

\onslide*<3>{
In practice: 
\begin{itemize}
\item \textcolor{darkbluesnd}{10}~DINCAE runs simultaneously on Abacus\\
(to \textit{saturate} both GPUs)
\item Approximately \textcolor{darkbluesnd}{2} hours per run\\
(depending on the number of epochs)
\item More than \textcolor{darkbluesnd}{300} runs for \textit{Calanus finmarchicus}\\
More than \textcolor{darkbluesnd}{200} runs for \textit{Calanus helgolandicus} (still running)
\end{itemize}
}

\end{frame}

\begin{frame}[t]
\frametitle{RMS (results - observations): Calanus finmarchicus}

Best combination in the blue circle\\
\footnotesize{Number of epochs = 2760, Learning rate = 0.00275, Laplacian penalty = 0.00011}

\begin{figure}
\centering
\includegraphics<1>[width=\textwidth]{epochs_laplacian_fin.jpg}
\includegraphics<2>[width=\textwidth]{epoch_learningrate_fin.jpg}
\includegraphics<3>[width=\textwidth]{laplacian_learningrate_fin.jpg}
\end{figure}
\end{frame}

\begin{frame}[t]
\frametitle{RMS (results - observations): Calanus helgolandicus}

Best combination in the blue circle\\
\footnotesize{Number of epochs = 2250, Learning rate = 0.00391, Laplacian penalty = 0.00069}

\begin{figure}
\centering
\includegraphics<1>[width=\textwidth]{epochs_laplacian_helgo.jpg}
\includegraphics<2>[width=\textwidth]{epoch_learningrate_helgo.jpg}
\includegraphics<3>[width=\textwidth]{laplacian_learningrate_helgo.jpg}
\end{figure}
\end{frame}


%------------------------------------------------------------------------------
\begin{frame}[t]
\frametitle{Results: \textit{Calanus finmarchicus}}

Comparison between the binned observations and the gridded field

\Wider{
\begin{figure}[h]
\centering
\begin{subfigure}[b]{0.49\textwidth}
\caption{Observations}
\includegraphics<1>[width=\textwidth]{binned_finmarchicus_log_00.jpg}
\includegraphics<2>[width=\textwidth]{binned_finmarchicus_log_01.jpg}
\includegraphics<3>[width=\textwidth]{binned_finmarchicus_log_02.jpg}
\includegraphics<4>[width=\textwidth]{binned_finmarchicus_log_03.jpg}
\includegraphics<5>[width=\textwidth]{binned_finmarchicus_log_04.jpg}
\includegraphics<6>[width=\textwidth]{binned_finmarchicus_log_05.jpg}
\includegraphics<7>[width=\textwidth]{binned_finmarchicus_log_06.jpg}
\includegraphics<8>[width=\textwidth]{binned_finmarchicus_log_07.jpg}
\includegraphics<9>[width=\textwidth]{binned_finmarchicus_log_08.jpg}
\includegraphics<10>[width=\textwidth]{binned_finmarchicus_log_09.jpg}
\includegraphics<11>[width=\textwidth]{binned_finmarchicus_log_10.jpg}
\includegraphics<12>[width=\textwidth]{binned_finmarchicus_log_11.jpg}
\includegraphics<13>[width=\textwidth]{binned_finmarchicus_log_12.jpg}



\end{subfigure}
\centering
\begin{subfigure}[b]{0.49\textwidth}
\caption{Analysis}
\includegraphics<1>[width=\textwidth]{calanus_finmarchicus_20231103T155830.jpg}
\includegraphics<2>[width=\textwidth]{calanus_finmarchicus_20231103T155830_month_01.jpg}
\includegraphics<3>[width=\textwidth]{calanus_finmarchicus_20231103T155830_month_02.jpg}
\includegraphics<4>[width=\textwidth]{calanus_finmarchicus_20231103T155830_month_03.jpg}
\includegraphics<5>[width=\textwidth]{calanus_finmarchicus_20231103T155830_month_04.jpg}
\includegraphics<6>[width=\textwidth]{calanus_finmarchicus_20231103T155830_month_05.jpg}
\includegraphics<7>[width=\textwidth]{calanus_finmarchicus_20231103T155830_month_06.jpg}
\includegraphics<8>[width=\textwidth]{calanus_finmarchicus_20231103T155830_month_07.jpg}
\includegraphics<9>[width=\textwidth]{calanus_finmarchicus_20231103T155830_month_08.jpg}
\includegraphics<10>[width=\textwidth]{calanus_finmarchicus_20231103T155830_month_09.jpg}
\includegraphics<11>[width=\textwidth]{calanus_finmarchicus_20231103T155830_month_10.jpg}
\includegraphics<12>[width=\textwidth]{calanus_finmarchicus_20231103T155830_month_11.jpg}
\includegraphics<13>[width=\textwidth]{calanus_finmarchicus_20231103T155830_month_12.jpg}
\end{subfigure}
\end{figure}
}
\end{frame}

%------------------------------------------------------------------------------
\begin{frame}[t]
\frametitle{Results: \textit{Calanus helgolandicus}}

Comparison between the binned observations and the gridded field

\Wider{
\begin{figure}[h]
\centering
\begin{subfigure}[b]{0.49\textwidth}
\caption{Observations}
\includegraphics<1>[width=\textwidth]{binned_helgolandicus_log_00.jpg}
\includegraphics<2>[width=\textwidth]{binned_helgolandicus_log_01.jpg}
\includegraphics<3>[width=\textwidth]{binned_helgolandicus_log_02.jpg}
\includegraphics<4>[width=\textwidth]{binned_helgolandicus_log_03.jpg}
\includegraphics<5>[width=\textwidth]{binned_helgolandicus_log_04.jpg}
\includegraphics<6>[width=\textwidth]{binned_helgolandicus_log_05.jpg}
\includegraphics<7>[width=\textwidth]{binned_helgolandicus_log_06.jpg}
\includegraphics<8>[width=\textwidth]{binned_helgolandicus_log_07.jpg}
\includegraphics<9>[width=\textwidth]{binned_helgolandicus_log_08.jpg}
\includegraphics<10>[width=\textwidth]{binned_helgolandicus_log_09.jpg}
\includegraphics<11>[width=\textwidth]{binned_helgolandicus_log_10.jpg}
\includegraphics<12>[width=\textwidth]{binned_helgolandicus_log_11.jpg}
\includegraphics<13>[width=\textwidth]{binned_helgolandicus_log_12.jpg}


\end{subfigure}
\centering
\begin{subfigure}[b]{0.49\textwidth}
\caption{Analysis}
\includegraphics<1>[width=\textwidth]{calanus_helgolandicus_20231106T152541.jpg}
\includegraphics<2>[width=\textwidth]{calanus_helgolandicus_20231106T152541_month_01.jpg}
\includegraphics<3>[width=\textwidth]{calanus_helgolandicus_20231106T152541_month_02.jpg}
\includegraphics<4>[width=\textwidth]{calanus_helgolandicus_20231106T152541_month_03.jpg}
\includegraphics<5>[width=\textwidth]{calanus_helgolandicus_20231106T152541_month_04.jpg}
\includegraphics<6>[width=\textwidth]{calanus_helgolandicus_20231106T152541_month_05.jpg}
\includegraphics<7>[width=\textwidth]{calanus_helgolandicus_20231106T152541_month_06.jpg}
\includegraphics<8>[width=\textwidth]{calanus_helgolandicus_20231106T152541_month_07.jpg}
\includegraphics<9>[width=\textwidth]{calanus_helgolandicus_20231106T152541_month_08.jpg}
\includegraphics<10>[width=\textwidth]{calanus_helgolandicus_20231106T152541_month_09.jpg}
\includegraphics<11>[width=\textwidth]{calanus_helgolandicus_20231106T152541_month_10.jpg}
\includegraphics<12>[width=\textwidth]{calanus_helgolandicus_20231106T152541_month_11.jpg}
\includegraphics<13>[width=\textwidth]{calanus_helgolandicus_20231106T152541_month_12.jpg}
\end{subfigure}
\end{figure}
}
\end{frame}


%-------------------------------------------------------------------------------------------

\begin{frame}[c]
\frametitle{Results: decadal periods}

\Wider{
\begin{figure}[h]
\centering
\begin{subfigure}[b]{0.49\textwidth}
\caption{Calanus finmarchicus}
\includegraphics<1>[width=\textwidth]{calanus_finmarchicus_20231103T155830_decade_01}
\includegraphics<2>[width=\textwidth]{calanus_finmarchicus_20231103T155830_decade_02}
\includegraphics<3>[width=\textwidth]{calanus_finmarchicus_20231103T155830_decade_03}
\includegraphics<4>[width=\textwidth]{calanus_finmarchicus_20231103T155830_decade_04}
\includegraphics<5>[width=\textwidth]{calanus_finmarchicus_20231103T155830_decade_05}
\includegraphics<6>[width=\textwidth]{calanus_finmarchicus_20231103T155830_decade_06}
\end{subfigure}
\centering
\begin{subfigure}[b]{0.49\textwidth}
\caption{Calanus helgolandicus}
\includegraphics<1>[width=\textwidth]{calanus_helgolandicus_20231106T152541_decade_01.jpg}
\includegraphics<2>[width=\textwidth]{calanus_helgolandicus_20231106T152541_decade_02.jpg}
\includegraphics<3>[width=\textwidth]{calanus_helgolandicus_20231106T152541_decade_03.jpg}
\includegraphics<4>[width=\textwidth]{calanus_helgolandicus_20231106T152541_decade_04.jpg}
\includegraphics<5>[width=\textwidth]{calanus_helgolandicus_20231106T152541_decade_05.jpg}
\includegraphics<6>[width=\textwidth]{calanus_helgolandicus_20231106T152541_decade_06.jpg}
\end{subfigure}
\end{figure}
}


\end{frame}

\begin{frame}
\frametitle{Time series of monthly averaged values}

Averaged values computed in region  40°N--60°N, 40°W--20°W.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{timeseries_calanus_finmarchicus_domain.jpg}
\end{figure}

\begin{itemize}
\item[\faPlus] Season cycle well reproduced,\\ even in periods without observations in the selected area
\item[\faPlus] Maximal values corresponding between observations and fields
\item[\faMinus] Lowest amplitude in the gridded field\\
(attributed to the smoothing and the difference in resolution) 
\end{itemize}
\end{frame}


%------------------------------------------------------------------------------
\begin{frame}
\frametitle{Future work}

\begin{enumerate}
\item Test other hyperparameters\\
(\textit{Random Search})
\item Better constrain the parameters values\\
(e.g. learning rate $<$ 0.001)
\item Check the influence of input data preparation:\\
transformation, quality control, using years before/after 
\item Add other environmental variable: distance to coast, chlorophyll-a concentration, \ldots
\end{enumerate}

\end{frame}

%------------------------------------------------------------------------------
\begin{frame}[t]
\frametitle{Future work (short-term)}

\begin{enumerate}
\item Finish the report\\
(essentialy complete the "Results" section)
\item Submit an abstract for IMDIS2024 conference\\ 
(to keep the momentum)
\end{enumerate}
\vspace*{-.5cm}
\begin{figure}[H]
\centering
\includegraphics[width=.8\textwidth]{imdis2024.png}
\end{figure}
\vspace*{-.5cm}
\textbf{Session:} PRODUCTS: Data products, information and knowledge\\
{\footnotesize
$\bullet$ Machine learning, Artificial Intelligence and application to data collection and data quality check
}
\end{frame}




\end{document}







